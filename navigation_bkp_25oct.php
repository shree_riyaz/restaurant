<?php
include_once("lib/language.php");
include_once("lib/class.authorise.php");
include_once("lib/dbfilter.php");
$language = new Language();
$lang = $language->english('eng');
$auth = new Authorise();
$DB = new DBFilter();
$user_id = $_SESSION['user_id'];
$module_name = $auth->getOneAccessibleModule($user_id);
$user_name = $DB->SelectRecord('users', "user_id='$user_id'");
// print_r($module_name[0]);exit;
?>

<div class="col-sm-3 nopadding ">
    <div class="list-tabs">
        <div class="logo-padding">
            <img src="images/logo.png" alt="" class="img-responsive center-block">
        </div>
<!--        --><?php //echo $_SESSION['usertype']; ?>
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav tabbing">

                    <?php
                    for ($i = 0; $i < count($module_name[0]); $i++) {
                        $module = $module_name[0][$i]->module_name;

                        if ($module == 'device') {
                            ?>
                            <li <?php if ($_REQUEST['mod'] == $module) { ?> 'active-tab' <?php } ?> ">
                                <a class="" href="<?php print CreateURL('index.php', 'mod=' . $module); ?>" data-toggle="dropdown">
                                    <i class="fa fa-tablet" aria-hidden="true"></i>
                                    <?php echo ucfirst($module) . "s"; ?>
                                </a>
                                <ul class="dropdown-menu">
                                    <li style="color: #3969ab; border-bottom: none;"><a
                                                href="<?php print CreateURL('index.php', 'mod=device'); ?>"><?php echo $lang['Devices'] ?></a>
                                    </li>
                                    <li style="color: #3969ab; border-bottom: none;"><a
                                                href="<?php print CreateURL('index.php', 'mod=device_locations'); ?>"><?php echo $lang['Device Locations'] ?></a>
                                    </li>
                                </ul>
                            </li>
                        <?php } else if ($module == 'faults') {
                            ?>
                            <li <?php if ($_REQUEST['mod'] == $module) { ?> class="active-tab" <?php } ?>>
                                <a class="" href="<?php print CreateURL('index.php', 'mod=faults&do=view&id=' . $service->service_id); ?>">
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i><?php echo $lang['Facility & Faults'] ?></a>
                            </li>
                            <?php
                        }
                        else if ($module == 'contact_us') {

                            if($_SESSION['usertype'] == 'company_admin'){
                            ?>

                            <li <?php if ($_REQUEST['mod'] == $module) { ?> class="active-tab" <?php } ?>>
                                <a class="" href="<?php print CreateURL('index.php', 'mod=contact_us'); ?>">
                                    <i class="fa fa-suitcase" aria-hidden="true"></i>Contact Us</a>
                            </li>

                        <?php } } else if ($module == 'role')//Added By : Neha Pareek, Dated : 30 Nov 2015
                        {
                            if (($_SESSION['usertype']) == 'super_admin') {
                                ?>
                                <li <?php if ($_REQUEST['mod'] == $module) { ?> class="active-tab" <?php } ?>>
                                    <a href="<?php print CreateURL('index.php', 'mod=role'); ?>">
                                        <i class="fa fa-suitcase" aria-hidden="true"></i>
                                        <?php echo $lang['Roles'] ?>

                                    </a>
                                </li>
                                <?php
                            } else {
                                continue;
                            }
                        } else if ($module != 'device_locations') {
//                            if()
//                            $icon =
                            ?>

                            <li  <?php if ($_REQUEST['mod'] == $module) { ?> class="active-tab" <?php } ?>>
                                <a class="li-opton-nav" href="<?php print CreateURL('index.php', 'mod=' . $module); ?>">
                                    <i class="fa fa-th-large" aria-hidden="true"></i>
                                    <?php
                                    if (($_SESSION['usertype']) == 'super_admin') {
                                        ###### convert singular to plural : For Module Names in Navigation (added by : Neha Pareek) ####
                                        if (substr($module, -1) != 'y') {
                                            echo ucfirst($module) . "s";
                                        } else {
                                            echo ucfirst(str_replace(substr($module, -1), 'ies', $module));
                                        }
                                    } else {
                                        if ($module != 'company') {
                                            echo ucfirst($module) . "s";
                                        } else {
                                            echo ucfirst('Dashboard');
                                            //echo str_replace(substr($module, -1), 'ies', $module);
                                            //ucfirst($module)."ies";
                                        }
                                    }
                                    ?>
                                </a>
                            </li>
                            <?php } } ?>

                </ul>
            </div>
        </nav>

    </div>
</div>

