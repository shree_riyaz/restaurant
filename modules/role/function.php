<?php

//========== Work for adding file ===============

if(isset($frmdata['addrole']))
{
	//add the role and the permissions to that role

	$err = '';
	if (trim($frmdata['role_name']) =='')
	{
		$err .= "Please enter role name.<br>";
	}
	else 
	{
		if(trim(validatename($frmdata['role_name']))== false) 
		{	
			$err .= "Only letters and white space allowed in role name field.<br>";
		}
		else
		{	// Added By Neha Pareek. Dated : 02 Nov 2015
			//echo $result = chkRolename($frmdata['role_name']);
			if(chkRolename(trim($frmdata['role_name']))==1)
			{
				$err .= "Role name already exist. Please use another role name.<br>";
			}
			else
			{	
				//$frmdata['role_name']= $frmdata['role_name'];		
				$frmdata['role_name']= $frmdata['role_name'];		
			}	
		}
	}

	if (empty($frmdata['modules']))
	{
		$err .= "Please define the module access.<br>";
	}
		
	if ($err == '')
	{
		//validation passed add the role and permission on their respective db tables
		//print_r($frmdata['role_name']); exit;
		$role_data = array('role_name'=>$frmdata['role_name']);
		$roleID = $DBFilter->InsertRecord('roles', $role_data);	//exit;	
		foreach($frmdata['modules'] as $moduleID => $value)
		{
			//add the read permission record 
			/*$permissionInfo = $DBFilter->SelectRecord('permission','permission_name="R"');
			$data['role_id'] = $roleID;
			$data['permission_id'] = $permissionInfo->permission_id;
			$data['module_id'] = $module_id;
			$DBFilter->InsertRecord('rolepermission',$data);*/
			
			if(is_array($value))
			{				
				//insert the add, edit permission records
				foreach ($value as $permission)
				{
					$permissionInfo = $DBFilter->SelectRecord('permission','permission_name="'.$permission.'"');
					$data['role_id'] = $roleID;
					$data['permission_id'] = $permissionInfo->permission_id;
					$data['module_id'] = $moduleID;
					$DBFilter->InsertRecord('rolepermission',$data);
				}	
			}			
		}
		//exit;
		$_SESSION['success'] = 'Role has been added successfully.'; 
		Redirect(CreateURL('index.php','mod=role')); die();
	}
	else
	{
		$_SESSION['error'] = $err; 
	} 
}

//============= Work for Editing file ================================
//====================================================================

if(isset($frmdata['editrole']))
{
	//edit the role and the permissions to that role
	
	$err = '';
	/*if (trim($frmdata['role_name']) =='')
	{
		$err .= "Please enter role name.<br>";
	}
	else 
	{
		if(trim(validatename($frmdata['role_name']))==false)
		{	
			$err .= "Only letters and white space allowed in role name field.<br>";
		}
		else
		{
			$frmdata['role_name']= $frmdata['role_name'];		
		}
	}*/
	
	// Added By Neha Pareek. Dated : 25 -01-2016
	if (trim($frmdata['role_name']) =='')
	{
		$err .= "Please enter role name.<br>";
	}
	else 
	{
		if(trim(validatename(trim($frmdata['role_name'])))== false)
		{	
			$err .= "Only letters and white space allowed in role name field.<br>";
		}
		else
		{
			//echo $result = changeRoleName(trim($frmdata['role_name']));
			if(changeRoleName(trim($frmdata['role_name'])) == 1)
			{
				$err .= "Role name already exist. Please use another name for role.<br>";
			}
			else
			{	
				$frmdata['role_name'] =  ucwords(trim($frmdata['role_name']));
			}	
		}
	}
	
	if (empty($frmdata['modules']))
	{
		$err .= "Please define the module access.<br>";
	}
	
	if ($err == '')
	{
		//validation passed add the role and permission on their respective db tables
	//echo 'in';
		$roleID = $_GET['id']; 		
		//print_r($frmdata); exit;
		$DBFilter->UpdateRecord('roles', $frmdata,'role_id = '.$roleID);
		// delete the old permission records for this role
		$DBFilter->DeleteRecord('rolepermission', 'role_id='.$roleID);
		//print_r($frmdata['modules']);
		foreach($frmdata['modules'] as $moduleID => $value)
		{
			//add the read permission record 
			/*$permissionInfo = $DBFilter->SelectRecords('permission','permission_name="R"');
			//echo '<pre>';print_r($permissionInfo);exit;
			$data['role_id'] = $roleID;
			$data['permission_id'] = $permissionInfo[0][0]->permission_id;
			$data['module_id'] = $moduleID;
			$DBFilter->InsertRecord('rolepermission',$data);*/
			
			if(is_array($value))
			{				
				//add the add, edit permission records
				foreach ($value as $permission)
				{
					$permissionInfo = $DBFilter->SelectRecords('permission','permission_name="'.$permission.'"');
					$data['role_id'] = $roleID;
					$data['permission_id'] = $permissionInfo[0][0]->permission_id;
					$data['module_id'] = $moduleID;
					$DBFilter->InsertRecord('rolepermission',$data);
				}	
			}
			else
			{
				//echo 'ok'; exit;
				$permissionInfo = $DBFilter->SelectRecord('permission','permission_name="R"');
				$data['role_id'] = $roleID;
				$data['permission_id'] = $permissionInfo->permission_id;
				$data['module_id'] = $moduleID;
				$DBFilter->InsertRecord('rolepermission',$data);
			}
					
		}//exit;	
		$_SESSION['success'] = 'Role has been updated successfully.'; 
		Redirect(CreateURL('index.php','mod=role')); die();
	}
	else
	{
		$_SESSION['error'] = $err; 
	} 
	
}


//============================================================================
//Delete the role
//============================================================================
if ($do == 'del')
{
	
	$roleAttached = $DBFilter->SelectRecord('users','role_id = '.$id);
	if ($roleAttached)
	{
		$_SESSION['error'] = 'Role could not be deleted. Role is assigned to one or more users.';
	}
	else
	{
		$data = array('is_deleted'=>'Y' , 'is_active'=>'N');
		$result = $DBFilter->UpdateRecord('roles', $data, "role_id ='$id'");
		$DBFilter->DeleteRecord('rolepermission','role_id = '.$id);
		$_SESSION['success'] = 'Role has been deleted successfully.';		
	}
	Redirect(CreateURL('index.php','mod=role'));die();
}


//======================================================================
//Assign Role
//======================================================================

if (isset($frmdata['assignrole']))
{
	//print_r($frmdata);exit;
	$err = '';
	if ($frmdata['role_id'] == '')
	{
		$err .= 'Please select role.<br>';
	}
	if ($frmdata['user_id'] == '')
	{
		$err .= 'Please select user.<br>';
	}
	
	if ($frmdata['fromDate'] != '' && $frmdata['toDate'] != '')
	{
		$frmdata['fromDate'] = str_replace('-','/',$frmdata['fromDate']);
	    $fromDate = strtotime($frmdata['fromDate']);
		$frmdata['toDate'] = str_replace('-','/',$frmdata['toDate']);
		$toDate = strtotime($frmdata['toDate']);
		if ($fromDate > $toDate)
		{
			$err .= "Please enter a valid date range.";
		}
	}
	if ($frmdata['user_id'] != '')
	{
		$exists = $DBFilter->SelectRecord('users','role_id !="" and role_id is not null and id ='.$frmdata['user_id']);
		if ($exists)
		{
			$err .= 'User already assigned a role.';
		}
	}
	if ($err == '')
	{
			
		if ($frmdata['fromDate'] != '')
		{
			 $frmdata['fromDate'] = date('m-d-Y',strtotime($frmdata['fromDate']));
		}
		if ($frmdata['toDate'] != '')
		{	
			 $frmdata['toDate'] = date('m-d-Y',strtotime($frmdata['toDate']));		
		}	
		$user = $frmdata['user_id'];
	//	print_r($frmdata);exit;
		$data =array('role_id'=>$frmdata['role_id'],'fromDate'=>$frmdata['fromDate'],'toDate'=>$frmdata['toDate']);
		$DBFilter->UpdateRecord('users',$data ,'id = '.$user);
		$_SESSION['success'] = 'Role has been assigned to the selected user.';
		Redirect(CreateURL('index.php','mod=role&do=adminusers'));die();
		
	}
	else
	{
		$_SESSION['error'] = $err;	
	}
}

?>