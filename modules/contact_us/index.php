<?php


if($_REQUEST['call']=='API' && $_REQUEST['do'] == 'contact')
{

    $permission = 'add';

}else
{
    $permission = $_SESSION['permission'];
}
$do = (isset($_REQUEST['do']) ? $_REQUEST['do'] : 'list');


$id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : '');

//print_r($do); exit;

include_once('function.php');

//echo "<pre>";
//print_r($services_res);
//print_r($faults_res);

// print_r($_SESSION);

switch ($do)
{
    case 'contact12':
        unset($_SESSION['usertype']);
        $CFG->template = "feedback/logout.php";
        break;

    case 'list':
        if ($frmdata['search'])
        {
            $frmdata['pageNumber'] = 1;
        }

        PaginationWork();
        $totalCount= 0;
        $Row = getContactList($totalCount);
//        echo "<pre>"; print_r($Row);
        $CFG->template = "contact_us/list.php";

        break;

    case 'contact':
        if($_REQUEST['call']=='API' && $_REQUEST['do']=='contact'&& $_REQUEST['mod']=='contact_us')
        {

            try{

            $result = addContactInfo($frmdata);
            if($result)
            {
                $final_data = array("message"=>"Your query added successfully","status"=>1);
                echo json_encode($final_data);
                exit;
            }
            else

                $final_data = array("message"=>"Query not added.try again","status"=>0);
            echo json_encode($final_data);
            exit;
            }
            catch (Exception $e){
                CreateLog($e->getMessage());
            }
        }

        break;
}

#### Function is used to show all feedbacks

function addContactInfo($data)
{

        date_default_timezone_set('Asia/Kolkata');

        global $DB, $frmdata,$DBFilter;
        $dev_loc_id = $DBFilter->SelectRecord("device_locations","location_id='".$data['location_id']."'");
        $cmp_id = $dev_loc_id->company_id;

        $device_data = $DBFilter->SelectRecords("device","company_id ='".$cmp_id."' and is_deleted = 'N' and is_active = 'Y' and device_id='".$data['device_id']."' order by device_id desc");
        if(count($device_data[0]) > 0)
        {

            $supervisor = $device_data[0][0]->user_id; //Supervisor Id

            $get_company_admin_email = $DBFilter->SelectRecords("company_detail"," is_deleted = 'N' and is_active = 'Y' and company_id=".$cmp_id);

            $company_admin_email = $get_company_admin_email[0][0]->company_email;
            $company_admin_name = $get_company_admin_email[0][0]->user_name;

            $data_contact = array('company_id'=>$cmp_id,'supervisor_id'=>$supervisor,'device_id'=>$data['device_id'],'location_id'=>$data['location_id'],'email'=>trim($data['email']),'contact_number'=>trim($data['contact_number']),'creation_date'=>date('Y-m-d h:i:s'));

            $add_contact_us = $DBFilter->InsertRecord('contact_us', $data_contact);
//            echo "<pre>"; print_r($add_contact_us); exit;

            if($add_contact_us)
            {
                if(isset($data['email']) && isset($data['contact_number']) != ''){
                    $name = ucwords($company_admin_name);
                    $user_info = isset($data['email']) ? $data['email'] : $data['contact_number'] ;
//                    $to = $company_admin_email;
                    $to = "riyaz.ahamad@sunarctechnologies.com";
                    $subject = "Facility System ";
                    $is_send = send_mail($data['email'],$user_info,$name);

                    if ($is_send){
//                        echo $is_send; exit;

                        $querys = "update contact_us set mail_status = 1 where id=" . $add_contact_us ;
                        $DBFilter->RunSelectQuery($querys);
                        $final_data = array("result"=>'Your query submitted successfully.',"status"=>1);
                        echo json_encode($final_data);
                        exit;
                    }else{

                        $final_data = array("result"=>'Your query submitted successfully.',"status"=>1);
                        echo json_encode($final_data);
                        throw new Exception('Oops! mail delivery failed while customer trying to connect with company admin.');
                        exit;

                    }
                }
            }else{

                $final_data = array("result"=>'Oops! Something went wrong while gethering contact info from you.',"status"=>0);
                echo json_encode($final_data);
                exit;
            }
        }
        else
        {
            $final_data = array("result"=>'Oops! No device found.',"status"=>0);
            echo json_encode($final_data);
            exit;
        }
}

/* function for updating task status using webservice*/
//include_once(CURRENTTEMP . "/index.php");

function getContactList(&$totalCount)
{
    global $DB, $frmdata,$DBFilter;
        $order = '';


    $query = "SELECT * FROM contact_us as cu ";
//    $query = $DBFilter->RunSelectQuery("SELECT * FROM contact_us cu");
//    $userss = $DBFilter->RunSelectQuery("SELECT * FROM contact_us where user_info !='' ");


    $where = "where company_id = ".$_SESSION['company_id'];
//        echo"<pre>";print_r($where);exit;

    if (trim($frmdata['keyword']) != '')
    {
        $frmdata['keyword'] = trim($frmdata['keyword']);

        $where .= " and (cu.email like  '%".$frmdata['keyword']."%' or cu.contact_number like  '%".$frmdata['keyword']."%' or dl.location_name like '%".$frmdata['keyword']."%')";
        $_SESSION['keywords'] ='Y';
    }
    else
    {
        $where.= '';
    }

    if (isset($frmdata['orderby']) && $frmdata['orderby'] != '')
    {
        $order.= " order by " . $frmdata['orderby'];
    }
    else
    {
        $order.= " order by cu.id desc ";
    }


    $query = $query.$where.$order;
//    $query = $query.$where;
//    echo '<pre>'; print_r($query); exit;

    $result = $DBFilter->RunSelectQueryWithPagination($query,$totalCount);
    return $result;

}
?>