<?php 
	
	$permission = $_SESSION['permission'];
	$do = (isset($_REQUEST['do']) ? $_REQUEST['do'] : 'list'); 
	$id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : '');
	if($do=='del' && $_SESSION['usertype']=='super_admin')
	 $permission = 'del';
	$userAlowedActions = array($permission);
	//print_r($userAlowedActions);
    if(!in_array($do,$userAlowedActions))	
	{
?> 
	<script type="text/javascript">
	alert('Your are not allowed to perform this action');
		document.location.href="<?php
		echo 'index.php?mod=plan'?>";
	</script>
   <?php
	   exit;
	}
	include_once('function.php');
    switch ($do)
    {
		case 'logout':
			unset($_SESSION['usertype']);
			$CFG->template = "plan/logout.php";			
			break;	
						
		case 'add':
			$CFG->template = "plan/add.php";
            break;
		case 'edit':
			$Row= $DBFilter->SelectRecord('plan', "plan_id='$id'");
			$CFG->template = "plan/edit.php";
            break;
		
        case 'list':
            if ($frmdata['search'])
            {
                $frmdata['pageNumber'] = 1;
            }
            PaginationWork();
            $totalCount= 0;
            $Row  = getPlan($totalCount);
            $CFG->template = "plan/list.php";
            break;
            
    }
    
    function getPlan(&$totalCount)
    {
        global $DB, $frmdata,$DBFilter;
		
        $query = " SELECT * from plan as p";
		//$where = " where p.is_deleted = 'N' and p.is_active='Y'";
		$where = " where p.is_deleted = 'N'";
		if (trim($frmdata['keyword']) != '')
        {
			$frmdata['keyword'] = trim($frmdata['keyword']);
			
            $where .= " and (p.plan_name like  '%".$frmdata['keyword']."%' or p.plan_description like  '%".$frmdata['keyword']."%'
				or  p.no_manager like '%". $frmdata['keyword']."%' or p.no_officer like '%".$frmdata['keyword']."%' or p.plan_price like'%". $frmdata['keyword']."%' )";
			$_SESSION['keywords'] = 'Y';
        }  
		else
		{
			$where.= '';
			//$where .= " where p.is_deleted = 'N' and p.is_active='Y'";
		}
			
        if (isset($frmdata['orderby']) && $frmdata['orderby'] != '')
        {
            $order .= " order by " . $frmdata['orderby'];
        }
        else
        {
            $order .= " order by p.plan_id desc";
        }
		
         $query = $query.$where.$order; 
  //echo $query;
        $result = $DBFilter->RunSelectQueryWithPagination($query,$totalCount);
        return $result;
    }
    

    include_once(CURRENTTEMP . "/index.php");
?>
