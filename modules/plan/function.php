<?php

    if (isset($frmdata['clearsearch']))
    {
        unset($frmdata);
        unset($_SESSION['pageNumber']);
        $CFG->template = "plan/list.php";
    }
    
    if (isset($frmdata['add_plan']))
    {
		$err = '';
		//print_r($frmdata);exit;
		if (trim($frmdata['plan_name']) == '')
		{
            $err .= "Please enter in plan name field.<br>";
			$frmdata['plan_name']='';
        }
		else
		{
			if(trim(validatename($frmdata['plan_name']))== false)
			{
				$err .= "Only letters and white space allowed in plan name field.<br>";
			}
			else
			{
				//Added By Neha Pareek. Dated : 22 Jan 2016
				//echo $result = chkPlanName($frmdata['plan_name']);
				if(chkPlanName(trim($frmdata['plan_name']))==1)
				{
					$err .= "Plan name already exist. Please use another Plan name.<br>";
				}
				else
				{	
					$frmdata['plan_name']= ucwords(trim($frmdata['plan_name']));		
				}
			}
		}
		
		if (trim($frmdata['plan_description']) == '')
		{
            $err .= "Please enter in plan description field.<br>";
			$frmdata['plan_description']='';
        }
		else
		{
			$frmdata['plan_description']= $frmdata['plan_description'];
		}
		
		if (trim($frmdata['no_manager']) == '')
		{
            $err .= "Please enter no. of supervisers.<br>";
			$frmdata['no_manager']='';
        }
		else
		{
			if(validatenum($frmdata['no_manager'])== true)
			{
				$frmdata['no_manager']= $frmdata['no_manager'];
			}
			else
			{
				$err .= "Only number is allowed in no. of superviser field.<br>";
			}
		}
		if (trim($frmdata['no_officer']) == '')
		{
            $err .= "Please enter no. of cleaners.<br>";
			$frmdata['no_officer']='';
        }
		else
		{
			if(validatenum($frmdata['no_officer'])== true)
			{
				$frmdata['no_officer']= $frmdata['no_officer'];
			}
			else
			{
				$err .= "Only numbers are allowed in no. of cleaner field.<br>";
			}
		}
		if (trim($frmdata['paid']) == '')
		{
            $err .= "Please select paid option.<br>";
			$frmdata['paid']='';
        }
		if($frmdata['paid']=='Y' && $frmdata['plan_price']=='')
		{
            $err .= "Please enter in plan price field.<br>";
			$frmdata['paid']='';
		}
		if ($err == '')
        {
			$nowdate = date("Y-m-d");
			$updatedate = date("Y-m-d H:i:s");
			//print_r($DBFilter->SelectRecord('device_locations',"location_id='".$frmdata['location_id']."'"));
			//print_r($frmdata);
			//exit;
			$data = array('plan_name'=>$frmdata['plan_name'], 'plan_description'=>$frmdata['plan_description'],'no_manager'=>$frmdata['no_manager'],'no_officer'=>$frmdata['no_officer'],'paid'=>$frmdata['paid'],'plan_price'=>$frmdata['plan_price']);
//			 print_r($data); exit;
			if ($DBFilter->InsertRecord('plan', $data))
            {
				
//				$pid=mysql_insert_id();

				$_SESSION['success'] = "Plan added successfully.";
                Redirect(CreateURL('index.php', 'mod=plan'));
                die();
            }
			else
            {
				$_SESSION['error'] = "<font color=red>Due to some reasion record could not be saved.</font>";
            }
        }
        else
        {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
        
    }
    elseif (isset($frmdata['update_plan'])) // Edit Record
    {
       $err = '';
		//print_r($frmdata);exit;
		if (trim($frmdata['plan_name']) == '')
		{
            $err .= "Please enter in plan name field.<br>";
			$frmdata['plan_name']='';
        }
		else
		{
			if(trim(validatename($frmdata['plan_name']))== false)
			{
				$err .= "Only letters and white space allowed in plan name field.<br>";
			}
			else
			{
				//Added By Neha Pareek. Dated : 22 Jan 2016
				//echo $result = chkPlanName($frmdata['plan_name']);
				if(changePlanName(trim($frmdata['plan_name']))==1)
				{
					$err .= "Plan name already exist. Please use another Plan name.<br>";
				}
				else
				{	
					$frmdata['plan_name']= ucwords(trim($frmdata['plan_name']));		
				}
			}
			/*if(validatename($frmdata['plan_name'])== true)
			{
				$frmdata['plan_name']= ucwords($frmdata['plan_name']);
			}
			else
			{
				$err .= "Only letters and white space allowed in plan name field.<br>";
			}*/
		}
		
		if (trim($frmdata['plan_description']) == '')
		{
            $err .= "Please enter in plan description field.<br>";
			$frmdata['plan_description']='';
        }
		else
		{
			$frmdata['plan_description']= $frmdata['plan_description'];
		}
		
		if (trim($frmdata['no_manager']) == '')
		{
            $err .= "Please enter no. of supervisers.<br>";
			$frmdata['no_manager']='';
        }
		else
		{
			if(validatenum($frmdata['no_manager'])== true)
			{
				$frmdata['no_manager']= $frmdata['no_manager'];
			}
			else
			{
				$err .= "Only number is allowed in no. of supervisers field.<br>";
			}
		}
		if (trim($frmdata['no_officer']) == '')
		{
            $err .= "Please enter no. cleaners.<br>";
			$frmdata['no_officer']='';
        }
		else
		{
			if(validatenum($frmdata['no_officer'])== true)
			{
				$frmdata['no_officer']= $frmdata['no_officer'];
			}
			else
			{
				$err .= "Only numbers are allowed in no. of cleaners field.<br>";
			}
		}
		if (trim($frmdata['paid']) == '')
		{
            $err .= "Please select paid option.<br>";
			$frmdata['paid']='';
        }
		if (trim($frmdata['paid']) == 'N')
		{
			unset($frmdata['plan_price']);
        }
		
		if($frmdata['paid']=='Y' && $frmdata['plan_price']=='')
		{
            $err .= "Please enter in plan price field.<br>";
			$frmdata['paid']='';
		}
		if($frmdata['paid']=='Y')
		{
			if(validatenum($frmdata['plan_price']) ==true)
			{
				$frmdata['plan_price']= $frmdata['plan_price']; }
			else
			{
				$err .= "Only numbers are allowed in price field.<br>";
			}
		}
		//print_r($frmdata);exit;
		if ($err == '')
        {
			$nowdate = date("Y-m-d");
			$updatedate = date("Y-m-d H:i:s");
			$data = array('plan_name'=>$frmdata['plan_name'], 'plan_description'=>$frmdata['plan_description'],'no_manager'=>$frmdata['no_manager'],'no_officer'=>$frmdata['no_officer'],'paid'=>$frmdata['paid'],'plan_price'=>$frmdata['plan_price']);
			// print_r($data); exit;
			 $DBFilter->UpdateRecord('plan', $data, "plan_id ='$id'");
			//print_r($data );exit;
			 $_SESSION['success'] = "Plan updated successfully.";
             Redirect(CreateURL('index.php', 'mod=plan'));
             die();
            
        }
        else
        {
            $_SESSION['error'] = $err;
        }
        
    }

    //============================================================================
        
    //Delete Multiple or Other Actions added by : Neha Pareek
        
    //============================================================================
    if($frmdata['actions'])
	{
		
		$total_chk= count($frmdata['chkbox']);// Count Selected Checkboxes
		$ids = $frmdata['chkbox'];
		if($frmdata['actions']=='Delete' || $frmdata['actions']=='Inactive') 
		{
			for($i = 0; $i < $total_chk; $i++) //Create Array of multiple checkbox id's
			{
				$id = $ids[$i];
				//$company = $DBFilter->SelectRecords('company_detail',"is_deleted='N' and is_active= 'Y' and plan_id=".$id);
				$company = $DBFilter->SelectRecords('company_detail',"is_deleted='N' and plan_id=".$id);
				if($company)
				{
					$_SESSION['error'] = 'Plan could not be deleted / inactive. Plan is assigned to one or more companies.';
					Redirect(CreateURL('index.php', 'mod=plan'));
					die();
				}
			}
		}
		if($frmdata['chkbox']=='' && isset($frmdata['actions']))
		{
			$_SESSION['error'] = 'Please select atleast one plan.';
		}
		else {	
			$ids = $frmdata['chkbox'];
			//print_r($ids);
			$action = $frmdata['actions']; 
			$cond = "plan_id=";
			multipleAction($ids,'plan',$action,$cond);
			$_SESSION['success'] = 'Plan has been '.$action.'d successfully.';
			Redirect(CreateURL('index.php', 'mod=plan'));
			die();
		} 
	}

    //============================================================================
        
    //Delete the Plan
        
    //============================================================================
    elseif ($do == 'del')
    {	
    	$companyAttached = $DBFilter->SelectRecord('company_detail','plan_id = '.$id);
		if ($companyAttached)
		{
			$_SESSION['error'] = 'Plan could not be deleted. Plan is assigned to one or more companies.';
		}
		else
		{
			$data = array('is_deleted'=>'Y','is_active'=>'N');
			$DBFilter->UpdateRecord('plan',$data,'plan_id = '.$id);
			$_SESSION['success'] = 'Plan has been deleted successfully.';		
		}
		Redirect(CreateURL('index.php','mod=plan'));
		die();
    }

?>