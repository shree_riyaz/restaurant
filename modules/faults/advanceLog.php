<?php
error_reporting(true);

/* basename function is used to get oly php filename for checking whether it is advance log or basic log */
$filename = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
$ROOTURL = "http://".$_SERVER['HTTP_HOST']."/iboks";
$ROOTPATH = $_SERVER['DOCUMENT_ROOT']."/iboks";	

require_once('tcpdf/customdb/db_pdo_mysql.php');
 $module = $_REQUEST['module'];
 $view = $_REQUEST['view'];
 $record = $_REQUEST['record'];
 // echo '<pre>';
// print_r($_SESSION);
/* check session for users and customers */
if($_SESSION['AUTHUSERID'] || $_SESSION['customer_sessionid'])
{
/*Custom code to show the start and end image of a particular ticket on the listing page */
$dp = new DB_PDO_MySQL();
$loggedinuser_lang = $dp->RunSelectQuery("SELECT language from vtiger_users where id= '".$_SESSION['AUTHUSERID']."'");
$loggedinuser_lang = $loggedinuser_lang[0][0]->language; 
require_once('lang.php');
$lang= en_to_nl($loggedinuser_lang);

// [AUTHUSERID] => 1
/*QUERY TO SELECT THE DETAILS OF A PROJECT*/
$selectproject = "SELECT 
vpcf.cf_753 as projecttype,vpcf.cf_755 as debitnumber,vpcf.cf_757 as alerterpcode,
vpcf.cf_761 as propertytype,vpcf.cf_765 as projectmemo,vpcf.cf_787 as street,
vpcf.cf_795 as planmap,vpcf.cf_799 as contactemail,vpcf.cf_835 as faxnumber,
vpcf.cf_845 as data,vpcf.cf_847 as command,vpcf.cf_849 as work,vpcf.cf_851 as reportto,vpcf.cf_855 as customernumber,
vpcf.cf_857 as complex,vpcf.cf_859 as yard,vpcf.cf_863 as mobilenumber,vpcf.cf_867 as callingcode,
vpcf.cf_869 as place,vpcf.cf_897 as customername,vpcf.cf_899 as clientnumber,vpcf.cf_901 as clientname,vpcf.cf_905 as contactpersonname,vpcf.cf_907 as jobnumber,vpcf.cf_911 as phone, vpcf.cf_968 as location,
vpcf.cf_970 as locationname,vpcf.cf_976 as namebuildingowner,vpcf.cf_978 as ownercomment,vpcf.cf_980 as address,
vpcf.cf_982 as phonenumber,vpcf.cf_984 as buildingowneremail,vpcf.cf_1053 as projectNumber,
vpcf.cf_1055 as zipcode,vpcf.cf_1057 as addressCompany,vpcf.cf_871 as landandpost, vpcf.cf_909 as locationname, vpcf.cf_787 as locationstreet, vpcf.cf_1148 as locationtown,vpcf.cf_1150 as locationphonenumber, vpcf.cf_863 as locationmobilenumber, vpcf.cf_835 as locationfaxnumber,vpcf.cf_1152 as locationemailid, vpcf.cf_913 as locationcontactpersonname,vpcf.cf_915 as locationcontactpersonphonenumber,vpcf.cf_793 as housenumber,
vtiger_project.projectname,vtiger_project.startdate, vtiger_project.targetenddate,vtiger_project.actualenddate,
vtiger_project.projectstatus,vtiger_project.projecttype,vtiger_crmentity.crmid,vtiger_crmentity.smownerid as assignedtoid,vpcf.cf_1098 as workorder,vtiger_crmentity.smcreatorid as creatorid, vtiger_crmentity.createdtime,vtiger_crmentity.label,vtiger_users.user_name, vtiger_users.first_name, vtiger_users.last_name,vtiger_groups.*,vtiger_contactscf.*,vtiger_contactscf.cf_1168 as town,vtiger_contactdetails.*
FROM vtiger_project
INNER JOIN vtiger_crmentity ON vtiger_project.projectid = vtiger_crmentity.crmid
left join vtiger_projectcf as vpcf on vtiger_project.projectid = vpcf.projectid
left join vtiger_contactdetails on vtiger_project.linktoaccountscontacts = vtiger_contactdetails.contactid 
left join vtiger_contactscf on vtiger_contactdetails.contactid = vtiger_contactscf.contactid
LEFT JOIN vtiger_users ON vtiger_crmentity.smcreatorid = vtiger_users.id
LEFT JOIN vtiger_groups ON vtiger_crmentity.smownerid = vtiger_groups.groupid 
WHERE vtiger_crmentity.deleted = 0 AND vtiger_crmentity.deleted != 1 AND vtiger_project.projectid = '".$record."' 
ORDER BY modifiedtime DESC"; 

$SELECTPROJECT = $dp->RunSelectQuery($selectproject);

$PROJECTDETAILS = $SELECTPROJECT[0][0];

/*Query to set the client data*/
$ClientData = $dp->RunSelectQuery("select vcontact.firstname,vacc.accountname as customerName,vacf.cf_1156 as customerNumber,vacf.cf_1158 as companyAddress,vacf.cf_1154 as clientNumber,vcf.cf_1164 as contactPersonCode,vacc.account_no from vtiger_project as vproj 
		inner join vtiger_crmentity as vcrm on vproj.projectid = vcrm.crmid
		left join vtiger_account as vacc on vproj.linktoaccountscontacts = vacc.accountid
		left join vtiger_accountscf as vacf on vacc.accountid = vacf.accountid
		left join vtiger_crmentityrel as vcrel on vproj.projectid = vcrel.crmid
		left join vtiger_contactdetails as vcontact on vcrel.relcrmid = vcontact.contactid
		left join vtiger_contactscf as vcf on vcontact.contactid = vcf.contactid
		where vcrm.crmid = '$record' and vcrm.deleted=0 and vcrel.relmodule= 'Contacts' order by 
        vcontact.contactid");
$count_clientData = count($ClientData[0]);

/* Query for Building Owner Information*/
$project_building_owner_info = $dp->RunSelectQuery("select vcontact.firstname,vcontact.email,vcontact.mobile, vc.description as comment,vcf.cf_1168 from vtiger_project as vproj
 inner join vtiger_crmentity as vcrm on vproj.projectid = vcrm.crmid
 left join vtiger_contactdetails as vcontact on vproj.contactid = vcontact.contactid
 left join vtiger_crmentity as vc on vcontact.contactid = vc.crmid
 left join vtiger_contactscf as vcf on vcontact.contactid = vcf.contactid
where vcrm.crmid='".$record."' and vcrm.deleted=0");
	
$project_building_owner_info = $project_building_owner_info[0][0];

/* Query only runs for advance log  */
if($view == 'advanceLog')
{
		/*QUERY TO SHOW THE WORKORDER OF PROJECT*/
		$PROJECTWORKORDERS = array();
		$WORKORDER = "SELECT DISTINCT vw.*,vwcf.*,vnote.*,va.*,vtiger_crmentity.* FROM vtiger_workorder as vw
		INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vw.workorderid
		INNER JOIN vtiger_crmentityrel ON (vtiger_crmentityrel.relcrmid = vtiger_crmentity.crmid OR vtiger_crmentityrel.crmid = vtiger_crmentity.crmid)
		LEFT JOIN vtiger_users ON vtiger_users.id = vtiger_crmentity.smownerid
		LEFT JOIN vtiger_groups ON vtiger_groups.groupid = vtiger_crmentity.smownerid
		left join vtiger_workordercf as vwcf on vw.workorderid = vwcf.workorderid
		left join vtiger_notes as vnote on vw.notesid = vnote.notesid
		left join vtiger_seattachmentsrel as vseattch on vnote.notesid = vseattch.crmid
		left join vtiger_attachments as va on vseattch.attachmentsid = va.attachmentsid
		WHERE vtiger_crmentity.deleted = 0 AND (vtiger_crmentityrel.crmid = '".$record."' OR vtiger_crmentityrel.relcrmid = '".$record."') ORDER BY vtiger_crmentity.crmid DESC";
			
		$PROJECTWORKORDER = $dp->RunSelectQuery($WORKORDER); 
		$PROJECTWORKORDERS[] = $PROJECTWORKORDER[0];


		/*QUERY TO SHOW THE DAILYENTRIES OF WORKORDER*/
			$ALLDAILYENTRY=array();
			foreach($PROJECTWORKORDER[0] as $key => $value)
			{
				$DAILYENTRIES = $dp->RunSelectQuery("select * from vtiger_dailyentry as vd
				left join vtiger_crmentity as vcrm on vcrm.crmid = vd.dailyentryid
				left join vtiger_workorder as vw on vw.workorderid = vd.workorderid 
				inner join vtiger_crmentity as vcrm1 on vcrm1.crmid = vw.workorderid
				inner join vtiger_crmentityrel as vcrmrel on vcrmrel.relcrmid = vw.workorderid
				inner join vtiger_dailyentrycf as vdcf on vdcf.dailyentryid = vd.dailyentryid
				inner join vtiger_users as vu on vu.id = vcrm.smcreatorid
				where vd.workorderid ='".$value->workorderid."'  and vcrm.deleted = 0 ");
				$ALLDAILYENTRY[$value->workorderid]=$DAILYENTRIES[0];
				
			}
			// echo '<pre>';
		// print_r($ALLDAILYENTRY);
		/*QUERY TO SHOW THE WORKORDER SIGNATUERE OF PROJECT*/
		$WORKORDERSIGNATURE = "select vwcf.cf_988 as person_name, vwcf.cf_990 as person_email, vg.groupname,vcrm.createdtime,
				vu.id as userid,vw.workorderid,vw.comments,vw.notesid, vnotes.filename, va.path, vattach.attachmentsid 
				from vtiger_workorder as vw
				LEFT JOIN vtiger_workordercf as vwcf ON vwcf.workorderid = vw.workorderid
				LEFT JOIN vtiger_crmentity as vcrm ON vcrm.crmid = vw.workorderid
				LEFT JOIN vtiger_crmentityrel as vcrmrel ON vcrmrel.relcrmid = vw.workorderid
				LEFT JOIN vtiger_users as vu ON vu.id = vcrm.smcreatorid
				LEFT JOIN vtiger_groups as vg ON vg.groupid = vcrm.smownerid
				LEFT JOIN vtiger_users2group as u2g ON u2g.groupid = vcrm.smownerid
				left join vtiger_notes as vnotes on vw.notesid = vnotes.notesid
				left join vtiger_seattachmentsrel as vattach on vnotes.notesid = vattach.crmid
				left join vtiger_attachments as va on vattach.attachmentsid = va.attachmentsid
				where vcrm.setype = 'WorkOrder' and vcrm.deleted = 0 and vw.projectid = '".$record."' 
				and vcrmrel.module = 'Project' and  vcrmrel.relmodule = 'WorkOrder' GROUP BY vw.workorderid";
		$PROJECTWORKORDERSIGNATURE = $dp->RunSelectQuery($WORKORDERSIGNATURE); 
		$PROJECTWORKORDERSIGNATURE = $PROJECTWORKORDERSIGNATURE[0][0];

		/*END OF WORKORDER SIGNATUERE  QUERY*/
}

/* Query used for showing project related plans */
$project_plan_data = "select vplan.planid,vplan.projectid,vpro.projectname,vplan.plan_title,vplan.plan_decription,
		vcrm.createdtime,vnotes.filename,vnotes.notesid,va.path,vattach.attachmentsid,vg.groupname
		from vtiger_plan as vplan
		inner join vtiger_crmentity as vcrm on vplan.planid = vcrm.crmid
		left join vtiger_crmentityrel as vcrel on vplan.planid = vcrel.relcrmid
		left join vtiger_project as vpro on vplan.projectid = vpro.projectid
		left join vtiger_notes as vnotes on vplan.notesid = vnotes.notesid
		left join vtiger_seattachmentsrel as vattach on vnotes.notesid = vattach.crmid
		left join vtiger_attachments as va on vattach.attachmentsid = va.attachmentsid
		left join vtiger_groups as vg on vg.groupid = vcrm.smownerid
		where vcrel.crmid = '".$record."' and vcrm.deleted = 0";
		
$PROJECTPLANS = $dp->RunSelectQuery($project_plan_data);
$COUNTPROJECTPLANS = count($PROJECTPLANS[0]);
$ALLTICKET = array();// intialization of array for ticket
$PLANRECORDINGIMAGE = array();

	foreach($PROJECTPLANS[0] as $key => $value)
	{	
		$selectprojecttask = "SELECT DISTINCT *,vu2.user_name as assignedTo,vtiger_users.user_name as createdBy	FROM vtiger_projecttask
				INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_projecttask.projecttaskid
				INNER JOIN vtiger_plan ON vtiger_plan.planid = vtiger_projecttask.planid
				LEFT JOIN vtiger_projecttaskcf ON vtiger_projecttaskcf.projecttaskid = vtiger_projecttask.projecttaskid
				LEFT JOIN vtiger_users ON vtiger_users.id = vtiger_crmentity.smcreatorid
				LEFT JOIN vtiger_users as vu2 on vtiger_crmentity.smownerid = vu2.id";
		
		if($_POST['typeOfWork'])
		{
			$where = " WHERE vtiger_crmentity.deleted = 0 AND vtiger_projecttaskcf.cf_1098 ='".$_POST['typeOfWork']."' and vtiger_plan.planid ='".$value->planid."'";
		
		}
		else
		{
			$where = " WHERE vtiger_crmentity.deleted = 0 AND vtiger_plan.planid ='".$value->planid."'";
		}
		$query = $selectprojecttask.$where;
		$SELECTPROJECTTASK = $dp->RunSelectQuery($query);
		$ALLTICKET[$value->planid] = $SELECTPROJECTTASK[0];

		$plan_recording_image =  $dp->RunSelectQuery("select vplan.plan_title,vn.filename as mainFilename,vn.notesid as mainNotesid,va.attachmentsid as mainAid ,va.path as mainPath ,vplan.planid as mainPlainid , recordImage.planid,recordImage.recImg,recordImage.recPath,recordImage.recAttachmentsid,recordImage.recNotesid from vtiger_plan as vplan 
				inner join vtiger_crmentityrel as vcrel on vplan.planid = vcrel.relcrmid
				left join vtiger_crmentity as vcrm on vplan.projectid = vcrm.crmid
				left join vtiger_notes as vn on vplan.notesid = vn.notesid
				left join vtiger_seattachmentsrel as vsea on vn.notesid = vsea.crmid
				left join vtiger_attachments as va on vsea.attachmentsid = va.attachmentsid
				left join (
				select vplan.planid as planid,vn.filename as recImg,va.path as recPath,vn.notesid as recNotesid,va.attachmentsid as recAttachmentsid from vtiger_plan as vplan 
				inner join vtiger_crmentityrel as vcrel on vplan.planid = vcrel.relcrmid
				left join vtiger_crmentity as vcrm on vplan.projectid = vcrm.crmid
				left join vtiger_senotesrel as vsn on vplan.planid = vsn.crmid
				left join vtiger_notes as vn on vsn.notesid = vn.notesid
				left join vtiger_seattachmentsrel as vsea on vn.notesid = vsea.crmid
				left join vtiger_attachments as va on vsea.attachmentsid = va.attachmentsid)
				as recordImage on vplan.planid = recordImage.planid
				where vcrel.crmid ='".$record."' and vcrm.deleted =0 and vplan.planid = '".$value->planid."'");
		$PLANRECORDINGIMAGE[$value->planid]=$plan_recording_image[0];
				
	}

//echo '<pre>'; print_r($PLANRECORDINGIMAGE);	
	/* Queries used for showing project's related question-answer , materials ,tools */
	$custom_array = $ALLTICKET; 
	$ALLTICKETMATERIAL=array();
	$ALLTICKETTOOL = array();
	$ALLTICKETQA = array();
	$STARTDOCUMENT = array();
	$ENDDOCUMENT = array();
	$PLANIMAGE = array();
	
	for($i=0;$i<$COUNTPROJECTPLANS;$i++)
	{	
		foreach($custom_array[$PROJECTPLANS[0][$i]->planid] as $key =>$data)
		{
			
			/* Plan Images for ticket */
			
			$plan_image =$dp->RunSelectQuery("select vplan.planid,vplan.projectid,vpro.projectname,vplan.plan_title,vplan.plan_decription,
			vcrm.createdtime,vnotes.filename,vnotes.notesid,va.path,vattach.attachmentsid,vg.groupname
			from vtiger_plan as vplan
			inner join vtiger_crmentity as vcrm on vplan.planid = vcrm.crmid
			left join vtiger_crmentityrel as vcrel on vplan.planid = vcrel.relcrmid
			left join vtiger_project as vpro on vplan.projectid = vpro.projectid
			left join vtiger_senotesrel as vsenote on vsenote.crmid = vplan.planid
			left join vtiger_notes as vnotes on vsenote.notesid = vnotes.notesid
			left join vtiger_seattachmentsrel as vattach on vnotes.notesid = vattach.crmid
			left join vtiger_attachments as va on vattach.attachmentsid = va.attachmentsid
			left join vtiger_groups as vg on vg.groupid = vcrm.smownerid
			where vcrel.relcrmid = '".$PROJECTPLANS[0][$i]->planid."' and vcrm.deleted = 0");
			
			$PLANIMAGE[$data->projecttaskid]=$plan_image[0];
					
			/*Query for ticket's start document*/
			$ticket_start_document = $dp->RunSelectQuery("select vn.*,vncf.*,va.* from vtiger_senotesrel as vsnote 
			inner join vtiger_crmentity as vcrm on vsnote.notesid = vcrm.crmid
			left join vtiger_seattachmentsrel as vattach on vsnote.notesid = vattach.crmid
			left join vtiger_notes as vn on vsnote.notesid = vn.notesid
			left join vtiger_notescf as vncf on vn.notesid = vncf.notesid
			left join vtiger_attachments as va on vattach.attachmentsid = va.attachmentsid
			where vcrm.deleted = 0 and vsnote.crmid ='".$data->projecttaskid."' and vncf.cf_895= 'Start Document'");
			
			$STARTDOCUMENT[$data->projecttaskid]=$ticket_start_document[0];

			/*Query for ticket's end document*/
			
			$ticket_end_document = $dp->RunSelectQuery("select vn.*,vncf.*,va.* from vtiger_senotesrel as vsnote 
			inner join vtiger_crmentity as vcrm on vsnote.notesid = vcrm.crmid
			left join vtiger_seattachmentsrel as vattach on vsnote.notesid = vattach.crmid
			left join vtiger_notes as vn on vsnote.notesid = vn.notesid
			left join vtiger_notescf as vncf on vn.notesid = vncf.notesid
			left join vtiger_attachments as va on vattach.attachmentsid = va.attachmentsid
			where vcrm.deleted = 0 and vsnote.crmid ='".$data->projecttaskid."' and vncf.cf_895= 'End Document'");
			
			$ENDDOCUMENT[$data->projecttaskid]=$ticket_end_document[0];

			/*Extra Images in a ticket */
			
			$ticket_extra_document = $dp->RunSelectQuery("select vn.*,vncf.*,va.* from vtiger_senotesrel as vsnote 
			inner join vtiger_crmentity as vcrm on vsnote.notesid = vcrm.crmid
			left join vtiger_seattachmentsrel as vattach on vsnote.notesid = vattach.crmid
			left join vtiger_notes as vn on vsnote.notesid = vn.notesid
			left join vtiger_notescf as vncf on vn.notesid = vncf.notesid
			left join vtiger_attachments as va on vattach.attachmentsid = va.attachmentsid
			where vcrm.deleted = 0 and vsnote.crmid ='".$data->projecttaskid."' and vncf.cf_895 = ''");
			
			$EXTRADOCUMENT[$data->projecttaskid]=$ticket_extra_document[0];
		
			
			if($view =='advanceLog')
			{
				/* Query for ticket_material */
				
				$ticket_material = $dp->RunSelectQuery("select vmlist.material_name,vm.material_number,vptask.projecttaskname from vtiger_materialmanagement as vm
				inner join vtiger_crmentity as vcrm on vm.projecttaskid = vcrm.crmid
				left join vtiger_crmentityrel as vcrel on vm.materialmanagementid = vcrel.relcrmid
				left join vtiger_projecttask as vptask on vm.projecttaskid = vptask.projecttaskid
				left join vtiger_materialslist as vmlist on vm.materialslistid = vmlist.materialslistid
				left join vtiger_materialmanagementcf as vmcf on vm.materialmanagementid = vmcf.materialmanagementid
				left join vtiger_materialslistcf as vmlistcf on vmlist.materialslistid =vmlistcf.materialslistid 
				where vm.projecttaskid='".$data->projecttaskid."' and vcrm.deleted = 0");
				
				$ALLTICKETMATERIAL[$data->projecttaskid]=$ticket_material[0];

				/* Query for ticket tool */	
				
				$ticket_tool = $dp->RunSelectQuery("select vtool.quantity,vtlist.tool_name from vtiger_tools as vtool
				inner join vtiger_crmentity as vcrm on vtool.projecttaskid= vcrm.crmid
				left join vtiger_crmentityrel as vcrel on vtool.toolsid = vcrel.relcrmid 
				left join vtiger_projecttask as vptask on vtool.projecttaskid = vptask.projecttaskid
				left join vtiger_toollist as vtlist on vtool.toollistid =vtlist.toollistid 
				where vtool.projecttaskid ='".$data->projecttaskid."' and vcrm.deleted = 0");
				
				$ALLTICKETTOOL[$data->projecttaskid]= $ticket_tool[0];
				
				/* Query for question answer of a ticket */
				
				$ticket_qa = $dp->RunSelectQuery("select vsq.select_question as question, if(vsqcf.cf_1096='Anders',vsqcf.cf_1100,vsqcf.cf_1096)as answer from vtiger_standardquestion as vsq 
					inner join vtiger_standardquestioncf as vsqcf on vsq.standardquestionid = vsqcf.standardquestionid
					inner join vtiger_crmentity as vcrm on vsq.standardquestionid = vcrm.crmid
					where vsq.projecttaskid ='".$data->projecttaskid."' and vcrm.deleted !=1");
					
					$ALLTICKETQA[$data->projecttaskid]=$ticket_qa[0];
				//echo	$count_qa = count($ALLTICKETQA);
			}
			
			else if($view == 'basicLog')
			{
				$ticket_qa = $dp->RunSelectQuery("select vsq.select_question as question, if(vsqcf.cf_1096='Anders',vsqcf.cf_1100,vsqcf.cf_1096)as answer from vtiger_standardquestion as vsq 
				inner join vtiger_standardquestioncf as vsqcf on vsq.standardquestionid = vsqcf.standardquestionid
				inner join vtiger_crmentity as vcrm on vsq.standardquestionid = vcrm.crmid
				where vsq.projecttaskid ='".$data->projecttaskid."' and vcrm.deleted !=1 
				and vsq.select_question in ('Doorvoeringen-Norm in minuten' ,'Bouwkundige werkzaamheden -Norm in minuten','Staal coating-Norm in minuten','Staal bekleding-Norm in minuten','Doorvoeringen-Bijzonderheden','Bouwkundige werkzaamheden-Bijzonderheden','Staal coating-Bijzonderheden','Staal bekleding-Bijzonderheden', 'Onderdelen brandveiligheid-Vrijeveld')");
				
				$ALLTICKETQA[$data->projecttaskid]=$ticket_qa[0];
			}
		}
	}
//echo "<pre>"; print_r($STARTDOCUMENT);
// echo '<pre>';print_r($PLANRECORDINGIMAGE);
// echo '<pre>';print_r($MATERIALLIST);
// echo '<pre>';print_r($ALLTICKETMATERIAL);
// echo '<pre>';print_r($ALLTICKETTOOL);
 // echo '<pre>';print_r($ALLTICKETQA);

$pdfbgpath = $ROOTURL;

?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<style type="text/css">
body{width:780px; margin:0px auto;}
</style>

<?php

// ---------------------------------------------------------
/*CSS STYLE*/
$dt = new DateTime();
$Timestamp = $dt->format('d:m:Y:H:i:s');
$pdf_file = 'projectlog_'.$Timestamp.'.pdf';
$html_file = 'projectlog_'.$Timestamp.'.html';
$noimageurl = $ROOTURL."/storage/pdfimage/No-Image-Available.jpg";

echo"<form method='post' action='temp/$pdf_file'>
<table width='32%' height='68' align='right' cellpadding='3' cellspacing='0' nobr='true'>
	<tr>
		<td height='33'  align='right' style='text-decoration:none; font-weight:bold'>
			<input type='submit' style='cursor:pointer' name='submit' id='submit_btn' value=''/>
			<div class='loading_div'></div>
		</td>
	</tr>
</table></form>
<div align='center'>
<table align='left' width='50%'>
<tr>
<td width='20%' valign='middle'>
<span><b>Select Type Of Work :</b></span>
</td>
<td width='80%' valign='middle'>
<form method='post'>
<select name='typeOfWork' onchange='this.form.submit()' id='typeOfWork' style='float:left;'>
    <option value='' selected='selected'>All</option>";
	if($_POST['typeOfWork']=='Doorvoeringen'){ 
	echo  "<option value='Doorvoeringen' selected='selected'>Doorvoeringen</option>";}
	else echo "<option value='Doorvoeringen'>Doorvoeringen</option>";
    if($_POST['typeOfWork']=='Bouwkundige werkzaamheden'){ 
	echo "<option value='Bouwkundige werkzaamheden' selected='selected'>Bouwkundige werkzaamheden</option>";}
	else echo "<option value='Bouwkundige werkzaamheden'>Bouwkundige werkzaamheden</option>";
    if($_POST['typeOfWork']=='Staalbekleding'){ 
	echo "<option value='Staalbekleding' selected='selected'>Staalbekleding</option>";}
	else echo "<option value='Staalbekleding'>Staalbekleding</option>";
    if($_POST['typeOfWork']=='Staalcoating'){
	echo "<option value='Staalcoating' selected='selected'>Staalcoating</option>";}
	else echo "<option value='Staalcoating'>Staalcoating</option>";
	 if($_POST['typeOfWork']=='Onderdelen brandveiligheid'){
    echo "<option value='Onderdelen brandveiligheid' selected='selected'>Onderdelen brandveiligheid</option>";}
	else echo "<option value='Onderdelen brandveiligheid'>Onderdelen brandveiligheid</option>";
	echo "</select></form>
  </td></tr>
</table></div>
";
$filedata .= "
<link rel='stylesheet' href='logcss.css'>
<div align='center' class='headerdiv' style='margin: 0; padding: 0;'>
<img src='storage/pdfimage/iboks-header.jpg' class='header' />
</div>
<br/><br/><br/>
<div align='center' class='containerdiv' style='margin: 0; padding: 0;'>
<img src='storage/pdfimage/boks logo.png' class='containerImg'/></div>
<div align='center' class='middlediv' style='margin: 0; padding: 0;'><h1>Welkom bij iBoks</h1>
<strong>uw digitale logboek</strong></div>
<br/>
<div align='center' width='100%' style='margin: 0; padding: 0;'>
<img src='storage/pdfimage/iboks-footer-info.jpg' class='footerInfo'/></div>
<div align='center' class='footerdiv' style='margin: 0; padding:30px 0px 0px 0px;'>
<img src='storage/pdfimage/iboks-footer.jpg' class='footer' /></div>";
$filedata .= "<br/><br/><br/>
<div align='center' class='headerdiv' style='margin: 0; padding: 0;'>
	<img src='storage/pdfimage/iboks-header.jpg' class='header' />
</div>
<div align='center' style='margin: 0; padding: 0;'>
	<span style='font-size:55px;'><b>".$lang['PROFESSIONALS']."</b></span><br/>
	<span style='font-size:50px; font-weight: 300;'>".$lang['IN BRANDPREVENTIE']."</span><br/>
	<br />
</div>
<div style='margin: 0; padding: 0;'>
	<p style='font-family: arial,helvetica,sans-serif; line-height: 15px; font-size: 12px;'>".$lang['pgone']."<br /><br />".$lang['pgtwo']."<br/><br />".$lang['pgthree']."<br/><br />".$lang['pgfour']."<br/><br />".$lang['pgfive']."<br/><br />".$lang['pgsix']."		
	</p>
</div>
<br/>
<div align='center' width='100%' style='margin: 0; padding: 0;'>
<img src='storage/pdfimage/iboks-footer-info.jpg' class='footerInfo'/></div>
<div align='center' class='footerdiv' style='margin: 0; padding:30px 0px 0px 0px;'>
<img src='storage/pdfimage/iboks-footer.jpg' class='footer' /></div>";
$filedata .="
<div align='center' style='width:100%'>
		<div class='projectTableDiv' align='center'>
			<table align='left' width='100%' height='150px'>
				<tr>
				<td colspan='4' align='left' height='50px'>". $lang['Project']."</td></tr>
				<tr>
				<th colspan='4' align='left' style='text-transform:capitalize'>".   $PROJECTDETAILS->projectname."</th>
				</tr>
				<tr>
				<th align='left'>". $lang['Start Date']."</th><th align='left'>". $lang['End Date']."</th><th align='left'>". $lang['Project Status']."</th><th align='left'>". $lang['Related To']."</th>
				</tr>
				<tr><td align='left'><span>".   $PROJECTDETAILS->startdate."</span></td>
				<td align='left'><span>".   $PROJECTDETAILS->actualenddate."</span></td>
				<td align='left'><span>".   $PROJECTDETAILS->projectstatus."</span></td>
				<td align='left'><span>".   $ClientData[0][0]->customerName."</span></td></tr>
				</table>
		</div>
</div>";


	$filedata .="<div align='center'>
	<div style='page-break-before:always;margin-top:20px;'>
	<table nobr='true' cellpadding='3' cellspacing='0' width='90%' height='700px' align='center' style='border-top-left-radius:5px;border-top-right-radius:5px;'>
	<tr>	
		<td colspan='4' align='left' height='50px'>
		<table nobr='true' cellpadding='3' cellspacing='0' width='100%'>
		<tr><td>
		<fieldset style='border-bottom:none; border-left:none; border-right:none; border-top:2px solid red;font-size:25px;color:red;'><legend>[". $lang['Basic Information']."]</legend></fieldset></td></tr>
		</table></td>
	</tr>	
		<tr>
			<td align='left' width='25%' ><span>".   $lang['Customer Number']."</span></td>
			<td align='left' width='25%'><span>".   $ClientData[0][0]->customerNumber ."</span></td>
			<td align='left' width='25%' ><span>".   $lang['Customer Name']."</span></td>
			<td align='left' width='25%' ><span>".   $ClientData[0][0]->customerName ."</span></td>
		</tr>
		<tr>
			<td align='left' width='25%' ><span>".   $lang['Client Number']."</span></td>
			<td align='left' width='25%' ><span>".   $ClientData[0][0]->clientNumber ."</span></td>
			<td align='left' width='25%' ><span>".   $lang['Address Company']."</span></td>
			<td align='left' width='25%'><span>".   $ClientData[0][0]->companyAddress ."</span></td>
		</tr>";
$filedata .="
	<tr>	
		<td colspan='4' align='left' height='50px'>
		<table nobr='true' cellpadding='3' cellspacing='0' width='100%'>
		<tr><td colspan='4'>
		<fieldset style='border-bottom:none; border-left:none; border-right:none; border-top:2px solid red;font-size:25px;color:red;'><legend>[". $lang['Location']."]</legend></fieldset></td></tr>
		</table></td>
	</tr>
	<tr>
		<td align='left' width='25%'><span>".   $lang['Land And Postcode']."</span></td>
		<td align='left' width='25%' ><span>".   $PROJECTDETAILS->landandpost ."</span></td>
		<td align='left' width='25%'><span>".   $lang['Name']."</span></td>
		<td align='left' width='25%'><span>".   $PROJECTDETAILS->locationname ."</span></td>
	</tr>
	<tr>
		<td align='left' width='25%' ><span>".   $lang['Street']."</span></td>
		<td align='left' width='25%'><span>".   $PROJECTDETAILS->locationstreet ."</span></td>
		<td align='left' width='25%' ><span>".   $lang['Town']."</span></td>
		<td align='left' width='25%'><span>".   $PROJECTDETAILS->locationtown ."</span></td>
	</tr>
	<tr>
		<td align='left' width='25%'><span>".   $lang['Phone']."</span></td>
		<td align='left' width='25%' ><span>".   $PROJECTDETAILS->locationphonenumber ."</span></td>
		<td align='left' width='25%'><span>".   $lang['Mobile']."</span></td>
		<td align='left' width='25%'><span>".   $PROJECTDETAILS->locationmobilenumber ."</span></td>
	</tr>
	<tr>
		<td align='left' width='25%'><span>".   $lang['Fax']."</span></td>
		<td align='left' width='25%' ><span>".   $PROJECTDETAILS->locationfaxnumber ."</span></td>
		<td align='left' width='25%'><span>".   $lang['Contact Email']."</span></td>
		<td align='left' width='25%'><span>".   $PROJECTDETAILS->locationemailid ."</span></td>
	</tr>
	<tr>
		<td align='left' width='25%'><span>".   $lang['Contact Person'] ."</span></td>
		<td align='left' width='25%'><span>".   $PROJECTDETAILS->locationcontactpersonname ."</span></td>
		<td align='left' width='25%'><span>".   $lang['Contact Phone']."</span></td>
		<td align='left' width='25%'><span>".   $PROJECTDETAILS->locationcontactpersonphonenumber ."</span></td>
	</tr>
	<tr>
		<td align='left' width='25%'><span>".   $lang['House Number']."</span></td>
		<td align='left' width='25%'><span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
		<td align='left' width='25%'><span>".   $PROJECTDETAILS->housenumber ."</span></td>
		<td align='left' width='25%'><span>&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	</tr>
	<tr>	
		<td colspan='4' align='left' height='50px'>
		<table nobr='true' cellpadding='3' cellspacing='0' width='100%'>
		<tr><td>
		<fieldset style='border-bottom:none; border-left:none; border-right:none; border-top:2px solid red;font-size:25px;color:red;'><legend>[". $lang['Owner Information']."]</legend></fieldset></td></tr>
		</table></td>
	</tr>
	<tr>
		<td align='left' width='25%'><span>".   $lang['Building Owner']."</span></td>
		<td align='left' width='25%' ><span>".  $project_building_owner_info->firstname ."</span></td>
		<td align='left' width='25%' ><span>".   $lang['Address']."</span></td>
		<td align='left' width='25%' ><span>".   $project_building_owner_info->cf_1168 ."</span></td>
	</tr>
	<tr>
		<td align='left' width='25%'><span>".   $lang['Phone Number']."</span></td>
		<td align='left' width='25%' ><span>".   $project_building_owner_info->mobile ."</span></td>
		<td align='left' width='25%'><span>Email</span></td>
		<td align='left' width='25%' ><span>".   $project_building_owner_info->email ."</span></td>
	</tr>
	<tr>	
		<td align='left' width='25%'><span>Comment</span></td>
		<td align='left' width='25%' colspan='3'><span>".   $project_building_owner_info->comment ."</span></td>
	</tr>";
	if($count_clientData==1)
	{
		if($ClientData)
		{
		$filedata .="
		<tr>	
		<td colspan='4' align='left' height='50px'>
		<table nobr='true' cellpadding='3' cellspacing='0' width='100%'>
		<tr><td>
		<fieldset style='border-bottom:none; border-left:none; border-right:none; border-top:2px solid red;font-size:25px;color:red;'><legend>[". $lang['Contact Person Detail']."-</legend></fieldset></td></tr>
		</table></td></tr>
		<tr class='heading'>
			<td align='left' width='25%'><span>S.No</span></td>
			<td align='left' width='25%'><span>".   $lang['Contact Person Name']."</span></td>
			<td align='left' width='25%' colspan='2'><span>".   $lang['Contact Person Code']."</span></td>
			
		</tr>";
		
		for($i=0;$i<$count_clientData;$i++)
		{
			$no = $i+1;
			$filedata .="
			<tr>
				<td align='left' width='25%'><span>".   $no ."</span></td>
				<td align='left' width='25%'><span>".   $ClientData[0][$i]->firstname ."</span></td>
				<td align='left' width='25%' colspan='2'><span>".   $ClientData[0][$i]->contactPersonCode ."</span></td>
				
			</tr> <br/>";
		}
		}
	
		else {
		$filedata .="
		<tr>	
			<td colspan='4' align='left' class='heading'><span>No Contact Person Detail found</span></td>
		</tr>";
		}

	}
$filedata .="
</table></div>
</div>
<br/>";
	if($count_clientData>1)
	{

		$filedata .="<div align='center' style='padding-top:50px;'>
		<table nobr='true' cellpadding='3' cellspacing='0' class='page' width='90%' height='400px' align='center' style='border-top-left-radius:5px;border-top-right-radius:5px;'>";
		if($ClientData)
		{
			$filedata .="
			<tr>	
			<td colspan='4' align='left' height='50px'>
			<table nobr='true' cellpadding='3' cellspacing='0' width='100%'>
			<tr><td>
			<fieldset style='border-bottom:none; border-left:none; border-right:none; border-top:2px solid red;font-size:25px;color:red;'><legend>[". $lang['Contact Person Detail']."-</legend></fieldset></td></tr>
			</table></td></tr>
			<tr class='heading'>
				<td align='left' width='25%'><span>S.No</span></td>
				<td align='left' width='25%'><span>".   $lang['Contact Person Name']."</span></td>
				<td align='left' width='25%' colspan='2'><span>".   $lang['Contact Person Code']."</span></td>
				
			</tr>";
			
			for($i=0;$i<$count_clientData;$i++)
			{
				$no = $i+1;
				$filedata .="
				<tr>
					<td align='left' width='25%'><span>".   $no ."</span></td>
					<td align='left' width='25%'><span>".   $ClientData[0][$i]->firstname ."</span></td>
					<td align='left' width='25%' colspan='2'><span>".   $ClientData[0][$i]->contactPersonCode ."</span></td>
				</tr> ";
			}
		}
		
		else {
		$filedata .="
		<tr>	
			<td colspan='4' align='left' class='heading'><span>No Contact Person Detail found</span></td>
		</tr>";
		}
		$filedata .="
		</table>
		</div>
<br/>";
	}


 
/**********************************************************************************************
COde added on 22 June 2015
Code is dedicated to run the ajax file to convert the plan pdf pages in image format and also to display on project log
Code developed by : Nidhishanker Modi
Responsible files to be noted down: tcpdf/process.php
***********************************************************************************************/
?>

<?php
	foreach($PROJECTPLANS[0] as $key => $value)
	{
		foreach($PLANRECORDINGIMAGE[$value->planid] as $key=>$value)
		{
			
			//Get the name of the file (Basename)
			$planid = $value->planid;
			 $main_file_name=	$value->mainFilename;
			//echo '<br/>';
			 $rec_file_name = $value->recImg;
			//echo '<br/>';
		
			//Get the prefix of the file (Prefix)
		//$filenameprefix = substr($rec_file_name, 0, strpos($rec_file_name, '_'));
			
			//Define the directory to store the uploaded PDF
			$pdfDirectory = $_SERVER['DOCUMENT_ROOT']."/iboks/";
			 
			if($rec_file_name)
			{
				//Define the directory to store the PDF Recording Preview Image
				$thumbDirectory = $_SERVER['DOCUMENT_ROOT']."/iboks/storage/planRecordingImage/";
				//define variables according to file name 	
				 $recording_document_filename = $value->recAttachmentsid."_".$value->recImg;
				//echo '<br/>';
				/*PATH SAVED IN DATABASE OF FILE AFTER DOCUMENT PATH AND BEFORE FILE NAME*/
				 $recording_document_path = $value->recPath;
			//	echo '<br/>';
				$pdf_filename = $recording_document_filename ;

				//Define the directory to store the uploaded PDF
				$pdfDirectory = $pdfDirectory.$recording_document_path.$recording_document_filename;
				//echo '<br/>';
				//Get the name of the file (Basename)
				$filename = basename($pdf_filename, ".pdf");

				//Clean the filename Remove all characters from the file name other than letters, numbers, hyphens and underscores
				$filename = preg_replace("/[^A-Za-z0-9_-]/", "", $filename).".pdf";
				//echo 'filenameagain '.$filenameecho. '<br/>';
				//Name the thumbnail image (Same as the pdf file -  You can set custom name)
				$thumb = basename($filename, ".pdf");

				//Add the desired extension to the thumbnail
				$thumbs = $thumb.".jpg";

				//count the page number of a pdf file 
				$pdftext = file_get_contents($pdfDirectory);
				$num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
				$recordedplanimage = array();
				if($num >= 1)
				{
					for($i=0; $i<$num; $i++)
					{
						$thumbnail = "page".$i."_".$thumbs; 
						// echo '<br/>';
						/*Remove the extention from the file name*/
						$widoutext= substr($thumbnail,0,strlen($thumbnail)-4);
						//explode the string
						$dimensionstr=substr($widoutext,strrpos($widoutext,"_")+1,strlen($widoutext));
						/*Given paramerters from filename exploding with "x" for width and height. Returning variable is an array with width and height*/
						$dimensionspdf = explode('x', $dimensionstr);
						/*Now width of image of plan first pdf page will be $dimensionspdf[0] and height will be $dimensionspdf[1]*/
						// echo "<pre>"; print_r($dimensionspdf);

						//execute imageMagick's 'convert', setting the color space to RGB This will create a jpg
						 exec("convert \"{$pdfDirectory}[$i]\" -colorspace RGB -geometry $dimensionspdf[0] $thumbDirectory$thumbnail");	
						// $thumbDirectory.$thumbnail; 
						
						$recordedplanimage[$planid][$i] = $thumbnail;
					}
				}
			}
			if($main_file_name)
			{
				//Define the directory to store the PDF Plan Preview Image
				$thumbDirectory = $_SERVER['DOCUMENT_ROOT']."/iboks/storage/planImage/";
							
				//define variables according to file name 	
				$main_document_filename = $value->mainAid."_".$value->mainFilename;
				// echo '<br/>';
				/*PATH SAVED IN DATABASE OF FILE AFTER DOCUMENT PATH AND BEFORE FILE NAME*/
			 	$main_document_path = $value->mainPath;
				//
				$pdf_filename = $main_document_filename ;
// echo '<br/>';
				//Define the directory to store the uploaded PDF
			 $pdfDirectory = $_SERVER['DOCUMENT_ROOT'].'/iboks/'.$main_document_path.$main_document_filename;
				//echo pdfDirectory.$main_document_path.$main_document_filename;
// echo '<br/>';
				//Get the name of the file (Basename)
				$main_filename = basename($pdf_filename, ".pdf");

				//Clean the main_filename Remove all characters from the file name other than letters, numbers, hyphens and underscores
				$main_filename = preg_replace("/[^A-Za-z0-9_-]/", "", $main_filename).".pdf";
	//echo 'filenameagain '.$filenameecho. '<br/>';
				//Name the thumbnail image (Same as the pdf file -  You can set custom name)
				$thumb = basename($main_filename, ".pdf");

				//Add the desired extension to the thumbnail
				$thumbs = $thumb.".jpg";

				//count the page number of a pdf file 
				$pdftext = file_get_contents($pdfDirectory);
				$num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
				$recordedplanimage = array();
				if($num >= 1)
				{
					for($i=0; $i<$num; $i++)
					{
						$thumbnail = "page".$i."_".$thumbs; 

						/*Remove the extention from the file name*/
						$widoutext= substr($thumbnail,0,strlen($thumbnail)-4);
						//explode the string
						$dimensionstr=substr($widoutext,strrpos($widoutext,"_")+1,strlen($widoutext));
						/*Given paramerters from filename exploding with "x" for width and height. Returning variable is an array with width and height*/
						$dimensionspdf = explode('x', $dimensionstr);
						/*Now width of image of plan first pdf page will be $dimensionspdf[0] and height will be $dimensionspdf[1]*/

						//execute imageMagick's 'convert', setting the color space to RGB This will create a jpg

						exec("convert \"{$pdfDirectory}[$i]\" -colorspace RGB -geometry 900 $thumbDirectory$thumbnail");
					$thumbDirectory.$thumbnail;// echo '<br/>';
						
						$recordedplanimage[$planid][$i] = $thumbnail;
					}
				}
			}
		}
	}
/*End of code*/	
// echo '<pre>';
// print_r($recordedplanimage);
// exit;
/**********************************************************************************************
COde added on 22 June 2015
Code is dedicated to display the project plan pdf pages images on project log
Code developed by : Jaishree & Nidhishanker
Responsible files to be noted down: tcpdf/process.php
***********************************************************************************************/
/*Recording Image Part */
$PDFIMAGEURL = "storage/planImage/";
$PDFRECORDIMAGEURL = "storage/planRecordingimage/";
// echo '<pre>';
		// print_r($PLANRECORDINGIMAGE);
		// echo '<br/>';

	foreach($PLANRECORDINGIMAGE as $key =>$value)
	{
		$planTitle = $value[0]->plan_title;
		$main_file_name = $value[0]->mainFilename;//echo '<br/>';
		$recording_file_name = $value[0]->recImg;//echo '<br/>';
		
		$modifiedpdfdata = $value[1];
		// echo '<pre>';
		// print_r($value);
		// echo '<br/>';
		$mainpdfdata = $value[0];
		
		if($recording_file_name)
		{
			 /*Get the file name */
			 $planpdffilename = $recording_file_name;
			 $planpdfwithattachmentid = $value[0]->recAttachmentsid.'_'.$value[0]->recImg;
			//	echo '<br/>';
			// Get the name of the file (Basename) without extention
			$filename = basename($planpdfwithattachmentid, ".pdf");

			/*Get the file prefix */
			$filenameprefix = substr($planpdffilename, 0, strpos($planpdffilename, '_'));

			//STORED PATH FROM DATABASE 	
			 $planpdfpath = $value[0]->recPath;
			//echo '<br/>';
		}
		else
		{
			/*Get the file name */
			 $planpdffilename = $main_file_name;
			 $planpdfwithattachmentid = $value[0]->mainAid.'_'.$value[0]->mainFilename;

			// Get the name of the file (Basename) without extention
			$filename = basename($planpdfwithattachmentid, ".pdf");

			/*Get the file prefix */
			$filenameprefix = substr($planpdffilename, 0, strpos($planpdffilename, '_'));

			//STORED PATH FROM DATABASE 	
			$planpdfpath = $value[0]->mainPath;

		}
		// ROOT PATH TO REACH AT UPLOADED PDF
		$pdfDirectory = $planpdfpath.$planpdfwithattachmentid;
		//echo '<br/>';
		if($filenameprefix == 'REC')
		{
			//Define the directory to store the recording PDF Preview Image
			$thumbDirectoryPath = "$ROOTPATH/storage/planRecordingImage/";
			$thumbDirectoryUrl = "storage/planRecordingImage/";
		}
		else
		{
			//Define the directory to store the main PDF Preview Image
			$thumbDirectoryPath = "$ROOTPATH/storage/planImage/";
			$thumbDirectoryUrl = "storage/planImage/";
		}
		//counT the page number of a pdf file 
		$pdftext = file_get_contents($pdfDirectory);
		$num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
		for($pgcnt = 0; $pgcnt < $num; $pgcnt++)
		{
			$generatedplanimagename = "page".$pgcnt."_".$filename.".jpg";
			//echo '<br/>';
			if($filename)
			{
				 $generatedplanimageurl = $thumbDirectoryUrl.$generatedplanimagename; 
				// echo '<br/>';
			}
			else
			{
				$generatedplanimageurl = $noimageurl;
			}

				//echo $generatedplanimageurl;
				 $filedata .= "
				 <div class='header'></div>
				 <div class='page'><table style='border-top-left-radius:5px;border-top-right-radius:5px;' align='center'><tr>
				<td align='center'><strong>".$planTitle."</strong></td></tr><tr><td align='center'>
				 <img src='".$generatedplanimageurl."' class='planPDFImage' style='width:700px'  />
				 </tr></table></div>"; 
		}
	}

/*End of code*/	


/***********************/
		/*For Plan Image and Recording Image's specific part according to ticket center*/
		
		foreach($custom_array as $key => $value)
		{	
			$TotalUSEDMATERIALS=array();
			foreach($value as $projecttaskid => $data)
			{
				$plan_title = $data->plan_title;
				$ticket_title = $data->projecttaskname;
				$stickernumber = $data->cf_1047;
				$typeofwork = $data->cf_1098;
				$assignedTo = $data->assignedTo;
				$createdBy = $data->createdBy;
				$stickerpoint = $data->cf_889;
				
				$point = str_replace(array( '{', '}' ),"",explode(",",$stickerpoint));
				// print_r($point);
				$startdate = ($data->startdate !='0000-00-00' ? date("d-m-Y", strtotime($data->startdate)) : '');
				$enddate = ($data->enddate !='0000-00-00' ? date("d-m-Y", strtotime($data->enddate)) : '');
				$startimage= $STARTDOCUMENT[$data->projecttaskid][0]->filename; 
				$startimageurl = $STARTDOCUMENT[$data->projecttaskid][0]->path.$STARTDOCUMENT[$data->projecttaskid][0]->attachmentsid."_".$STARTDOCUMENT[$data->projecttaskid][0]->filename;
				
				if($STARTDOCUMENT[$data->projecttaskid][0]->attachmentsid)
				$startimagepath  = "$startimageurl";
				else $startimagepath = $noimageurl;
				$endimage= $ENDDOCUMENT[$data->projecttaskid][0]->filename;
				$endimageurl = $ENDDOCUMENT[$data->projecttaskid][0]->path.$ENDDOCUMENT[$data->projecttaskid][0]->attachmentsid."_".$ENDDOCUMENT[$data->projecttaskid][0]->filename;
				if($ENDDOCUMENT[$data->projecttaskid][0]->attachmentsid)
				$endimagepath  = "$endimageurl";
				else $endimagepath = $noimageurl;
				/*Plan per page image */
				
				$count_planImage = count($PLANIMAGE[$data->projecttaskid]);

				/*Extra Images in a ticket */
				$ticket_extra_document = $dp->RunSelectQuery("select vn.*,vncf.*,va.* from vtiger_senotesrel as vsnote 
				inner join vtiger_crmentity as vcrm on vsnote.notesid = vcrm.crmid
				left join vtiger_seattachmentsrel as vattach on vsnote.notesid = vattach.crmid
				left join vtiger_notes as vn on vsnote.notesid = vn.notesid
				left join vtiger_notescf as vncf on vn.notesid = vncf.notesid
				left join vtiger_attachments as va on vattach.attachmentsid = va.attachmentsid
				where vcrm.deleted = 0 and vsnote.crmid ='".$data->projecttaskid."' and vncf.cf_895 = ''");
				
				$EXTRADOCUMENT[$data->projecttaskid]=$ticket_extra_document[0];
				
				$EXTRAIMAGE = $EXTRADOCUMENT[$data->projecttaskid][0]->path."".$EXTRADOCUMENT[$data->projecttaskid][0]->attachmentsid."_".$EXTRADOCUMENT[$data->projecttaskid][0]->filename;
				

		foreach($PLANIMAGE[$data->projecttaskid] as $key=>$value)
		{
			for($i=0; $i<count($value); $i++)
			{
				$plan_document_filename = $value->attachmentsid."_".$value->filename;
				$plan_document_path = $value->path;
				$plan_document = $value->filename;
			
				$filenameprefix = substr($plan_document, 0, strpos($plan_document, '_'));

				if($filenameprefix == 'REC')
				{
					/*Extra code to generate sticker*/
					if($pgno)
					$pgno = $pgno-1;
					else $pgno = 0;
				
					/*PDF name*/
					$pdf_filename = "page".$pgno.'_'.$plan_document_filename ;

					/*pdf name with extention .jpg except .pdf*/
					$img= strtr($pdf_filename, array('..' => '.','pdf' => 'jpg'));

					/*Plan pdf images path*/
					$pdfimagepath = 'storage/planRecordingImage/';	
					/*Url to get the plan pdf image file*/
					$plandocumenturl = $ROOTURL."/".$pdfimagepath.$img; 

					/*Url path to save the sticker of plan pdf*/
					$IMAGEROOT =$ROOTPATH."/storage/stickerimage/";

					/*Generated plan sticker name*/
					$filenamefull = intval($point[0])."".intval($point[1])."_".$img;

					/*full document path to save to the generated sticker image*/
					$filenamefullpath = $ROOTPATH."/storage/stickerimage/".$filenamefull;

					$wmax = 280; //width of sticker to be
					$hmax = 240; //height of sticker to be

					/*Function call for iboks plan image sticker creation */
					IBOKS_PLAN_STICKER($plandocumenturl, $filenamefullpath, $wmax, $hmax, $point); 

					/*Get the generated sticker image URL*/
					$filenamefullurl = "storage/stickerimage/".$filenamefull;
				}
			}
		}
			
 $filedata .= "
 <div>
 <table nobr='true' cellpadding='6' cellspacing='0' width='90%' style='border-top-left-radius:5px;border-top-right-radius:5px;page-break-inside: avoid;' align='center'>
	<tr>
		<td colspan='3'>
		<table nobr='true' cellpadding='3' cellspacing='0' width='100%'>
		<tr><td>
		<fieldset style='border-bottom:none; border-left:none; border-right:none; border-top:2px solid red;font-size:25px;color:red;'>
		<legend>[".  $lang['Details of Ticket'].' '.$ticket_title."]</legend></fieldset></td></tr>
		</table></td>
	</tr>
	<tr class='heading'>
		<td align='right' width='49%'><span>".  $lang['Plan Image']."</span></td>
		<td width='2%' align='center'></td>
		<td align='left' width='49%'><span>".  $lang['Recording Image']."</span></td>
	</tr>
	<tr>
		<td align='right'>
			<img src=".$filenamefullurl." class='planImage' />
		</td>
		<td width='1%' align='center'></td>
		<td align='left'>
			<img src='".  $EXTRAIMAGE ."' class='planImage'  />
		</td>
	</tr>
	<tr class='heading'>
		<td align='right'><span>".  $lang['Start Image']."</span></td>
		<td width='2%' align='center'></td>
		<td align='left'><span>".  $lang['End Image']."</span></td>
	</tr> 
	<tr>
		<td align='right'><img src='".  $startimagepath ."' class='planImage' /></td>
		<td width='1%' align='center'></td>
		<td align='left'><img src='".  $endimagepath ."' class='planImage'  />
		</td>
	</tr>

	<tr>
	<td colspan=3>
	<table cellpadding='3' cellspacing='0' nobr='true' width='100%'>
	<tr>
		<td align='right' width='28%'><span>".  $lang['Plan Title']."</span></td>
		<td width='2%' align='center'></td>
		<td align='left' width='70%'><span>".  $plan_title."</span></td>
	</tr>
	<tr>
		<td align='right'><span>".  $lang['Recording Date']."</span></td>
		<td width='1%' align='center'></td>
		<td align='left'><span>".  $startdate."</span></td>
	</tr>
	<tr>
		<td align='right'><span>".  $lang['Name Employee Recorder']."</span></td>
		<td width='1%' align='center'></td>
		<td align='left'><span>".  $createdBy."</span></td>
	</tr>
	<tr>
		<td align='right'><span>".  $lang['Execution Date']."</span></td>
		<td width='1%' align='center'></td>
		<td align='left'><span>".  $enddate."</span></td>
	</tr>
	<tr>
		<td align='right'><span>".  $lang['Name Employee Machanic']."</span></td>
		<td width='1%' align='center'></td>
		<td align='left'><span>".  $assignedTo."</span></td>
	</tr>
	<tr>
		<td align='right'><span>".  $lang['Sticker Number']."</span></td>
		<td width='1%' align='center'></td>
		<td align='left'><span>".  $stickernumber."</span></td>
	</tr>
	<tr>
		<td align='right'><span>".  $lang['Type Of Work']."</span></td>
		<td width='1%' align='center'></td>
		<td align='left'><span>".  $typeofwork."</span></td>
	</tr>";

		/*Ticket Q/A list */
					$count_qa = count($ALLTICKETQA[$data->projecttaskid]);
					if($count_qa!=0)
					{
					
						 $filedata .= "<tr>
							<td align='right' width='28%'>
								<span>".   $lang['Detail Of Question Answer']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
							<td width='2%' align='center'></td>
							<td  align='left' width='70%' style='padding-left: 24px;'>
								<table width='100%' cellpadding='3' cellspacing='0' nobr='true' align='center' class='tdborder'>
									<tr class='heading'>
										<td>
											<span>".   $lang['Question']."</span></td>
										<td>
											<span>".   $lang['Answer']."</span></td>
									</tr>";
				 		
							foreach($ALLTICKETQA[$data->projecttaskid] as $key=>$value)
							{
								for($i=0; $i<count($value); $i++)
								{
									$question = $value->question;
									$answer = $value->answer; 
								
									 $filedata .= "<tr>
										<td width='70%' class='tdborder'>
											<span>".   $question ."</span></td>
										<td width='30%' class='tdborder'>
											<span>".   $answer ."</span></td>
									</tr>";
						
								}
							} 
					
					 $filedata .= "	</table></td>
						</tr>";
					
					}
					/* End of Ticket Q/A list */

					/*TICKET MATERIAL QUERY*/
					$count_material = count($ALLTICKETMATERIAL[$data->projecttaskid]);
					if($count_material!=0)
					{
					 $filedata .= "
						<tr>
							<td align='right' width='28%'><span>".  $lang['Detail Of Used Material']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
							<td width='2%' align='center'></td>
							<td  align='left' width='70%' style='padding-left: 24px;'>
								<table width='100%' cellpadding='3' cellspacing='0' nobr='true' align='center' class='tdborder'>
									<tr class='heading'>
										<td width='70%' class='tdborder'><span>".  $lang['Material Name']."</span></td>
										<td width='30%' class='tdborder'><span>".  $lang['Quantity']."</span></td>
									</tr>";
									
								foreach($ALLTICKETMATERIAL[$data->projecttaskid] as $key=>$value)
								{			//echo $key;
									for($i=0; $i<count($value); $i++)
									{
										$mateialName = $value->material_name;
										$materialQuantity = $value->material_number; 
							
								 $filedata .= "	<tr>
										<td width='70%' class='tdborder'><span>".  $mateialName ."</span></td>
										<td width='30%' class='tdborder'><span>".  $materialQuantity ."</span></td>
									</tr>";
		
									}
								}
							
							 $filedata .= "  </table>
						  </td>
						</tr>";
					
					}
					/*END OF TICKET MATERIAL QUERY*/
					
					/*Ticket tool list */
					$count_tool = count($ALLTICKETTOOL[$data->projecttaskid]);
					if($count_tool!=0)
					{
					 $filedata .= "
						<tr>
							<td align='right' width='28%'>
								<span>".   $lang['Detail Of Used Tools']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
							<td width='2%' align='center'></td>
							<td  align='left' width='70%' style='padding-left: 24px;'>
								<table width='100%' cellpadding='3' cellspacing='0' nobr='true' align='center' class='tdborder'>
									<tr class='heading'>
										<td width='70%' class='tdborder'>
											<span>".   $lang['Tool Name']."</span>
										</td>
										<td width='30%' class='tdborder'>
											<span>".   $lang['Quantity']."</span>
										</td>
									</tr>";
							
							foreach($ALLTICKETTOOL[$data->projecttaskid] as $key=>$value)
							{			//echo $key;
								for($i=0; $i<count($value); $i++)
								{
									$toolName = $value->tool_name;
									$quentity = $value->quantity; 
									
							
								 $filedata .= "	<tr>
										<td width='70%' class='tdborder'>
											<span>".  $toolName ."</span>
										</td>
										<td width='30%' class='tdborder'>
											<span>".  $quentity ."</span>
										</td>
									</tr>";
						
								}
							} 
					
							 $filedata .= "  </table>
						  </td>
						</tr>";
				
					}
					/*End of Ticket tool list */
			
	
 $filedata .= "</table>
 </td></tr></table>";


$TotalUSEDMATERIALS=$ALLTICKETMATERIAL;
			}
			
		}	

/* template for all the materials used in project */
if($view == 'advanceLog')
{
if($PROJECTDETAILS->workorder != 'Onderdelen brandveiligheid')
{

	if(count($TotalUSEDMATERIALS) > 0)
	{


		 $filedata .= "<div style='page-break-before:always;' align='center'>
		 <table nobr='true' cellpadding='3' cellspacing='0' width='90%'>
			<tr><td height='50px'>
			<fieldset style='border-bottom:none; border-left:none; border-right:none; border-top:2px solid red;font-size:25px;color:red;'>
			<legend>[".   $lang['Detail Of Used Material In Project'] ."]</legend></fieldset></td></tr>
			</table>
			<table nobr='true' cellpadding='3' cellspacing='0' class='border' width='90%' align='center'>
			<tr class='heading'>	
					<td align='left'><span>".   $lang['Material Name']."</span></td>
					<td align='left'><span>".   $lang['Quantity']."</span></td>
					<td align='left'><span>".   $lang['Used In']."</span></td>
			</tr>";

	foreach($TotalUSEDMATERIALS as $MATERIALLIST)
	{
		for($i=0;$i<count($MATERIALLIST);$i++)
		{
			$materialName = $MATERIALLIST[$i]->material_name;
			$materialQuantity = $MATERIALLIST[$i]->material_number;
			$usedInTicket = $MATERIALLIST[$i]->projecttaskname;
	
		 $filedata .= "	<tr>	
					<td align='left'><span>".  $materialName."</span></td>
					<td align='left'><span>".  $materialQuantity ."</span></td>
					<td align='left'><span>".  $usedInTicket ."</span></td>
			</tr>";
	
		}
	}	
	 $filedata .= "</table></div>";
	}
}
}
if($view == 'advanceLog')
{
	if($PROJECTWORKORDERS[0] == '')
	{

		 $filedata .= "<h4 align='center'>".$lang['No Work Order Found']."</h4>";
	
	}
	else 
	{
		 $filedata .= "<div class='page'><h2 align='center'>".$lang['Work Order']."</h2>";
		 $filedata .= "<table nobr='true' cellpadding='3' cellspacing='0' align='center' width='90%' style='border-top-left-radius:5px;
		border-top-right-radius:5px;'>";
		
			foreach($PROJECTWORKORDERS[0] as $key=> $value)
			{
				$name= $value->employe_number;
				$originalDate = $value->cf_1172;
				$newDate = date("d-m-Y", strtotime($originalDate));
				$personName = $value->cf_988;
				$personEmail = $value->cf_990;
				$signatureDocument = $value->attachmentsid.'_'.$value->filename;
				if($value->filename)
					$signaturePath =  $value->path.$value->attachmentsid.'_'.$value->filename;
				else
					$signaturePath =  "storage/pdfimage/No-Image-Available.jpg";
	
				$no=$key+1;
 $filedata .= "
				<tr>	
					<td align='left' colspan='7' class='heading'>
						 <table nobr='true' cellpadding='3' cellspacing='0' width='100%'>
			<tr><td height='50px'>
			<fieldset style='border-bottom:none; border-left:none; border-right:none; border-top:2px solid red;font-size:25px;color:red;'>
			<legend>[".  $lang['Detail Of Work Order'].' '.$name."]</legend></fieldset></td></tr>
			</table>	</td>		</tr>
				<tr class='heading'>
					<td align='left' width='20%'><span>".  $lang['Work Order No']."</span></td>
					<td align='left' width='20%'><span>".  $lang['Date']."</span></td>
					<td align='left' width='20%' colspan='2'><span>".  $lang['Person Name']."</span></td>
					<td align='left' width='20%' colspan='2'><span>".  $lang['Person Email']."</span></td>
					<td align='left' width='20%'><span>".  $lang['Signature Document']."</span></td>
				</tr>
				<tr>
					<td align='left' width='20%'><span>".  $no ."</span></td>
					<td align='left' width='20%'><span>".  $newDate ."</span></td>
					<td align='left' width='20%' colspan='2'><span>". $personName ."</span></td>
					<td align='left' width='20%' colspan='2'><span>".  $personEmail ."</span></td>
					<td align='left' width='20%'>
					<a href='".  $signaturePath ."' target='_blank'>
						<img src='".  $signaturePath ."' height='100' width='100' style='margin-top:20px;' align='center'/>
						</a>
					</td>
				</tr>";

		/* Daily Entry Detail */
					$count_dailyEntry = count($ALLDAILYENTRY[$value->workorderid]);
					if($count_dailyEntry!=0)
					{
					 $filedata .= "
						<tr>
							<td align='left' width='50%' colspan='7' class='heading'>
								<span><span>".  $lang['Daily Entry Details']."</span></span>
							</td>
						</tr>
						<tr>
							<td colspan='7'>
							<table width='100%' cellpadding='3' cellspacing='0' nobr='true' class='tdborder'>
							<tr height='40px' class='heading'>
							<td align='left' class='tdborder'><span>".  $lang['Working Hours']."</span></td>
							<td align='left' class='tdborder'><span>".  $lang['Reason']."</span></td>
							<td align='left' class='tdborder'><span>".  $lang['Waiting Hours']."</span></td>
							<td align='left' class='tdborder'><span>".  $lang['Waiting Hours Reason']."</span></td>
							<td align='left' class='tdborder'><span>".  $lang['Parking Fees']."</span></td>
							<td align='left' class='tdborder'><span>".  $lang['Created By']."</span></td>
							<td align='left' class='tdborder'><span>".  $lang['Date']."</span></td>
							</tr>";
							
							foreach($ALLDAILYENTRY[$value->workorderid] as $key=>$value)
							{			//echo $key;
								for($i=0; $i<count($value); $i++)
								{
									$workingHours = $value->working_hours;
									$reason = $value->reason;
									$waitingHours = $value->waiting_hours;
									$waitingHoursReason = $value->waiting_hour_reason;
									$parkingFee = $value->parking_fee;
									$createdBy = $value->user_name;
									//$date = $value->cf_1120;
							 $filedata .= "
									<tr height='40px'>
										<td align='left' class='tdborder'><span>".   $workingHours ."</span></td>
										<td align='left' class='tdborder'><span>".   $reason ."</span></td>
										<td align='left' class='tdborder'><span>".   $waitingHours ."</span></td>
										<td align='left' class='tdborder'><span>".   $waitingHoursReason ."</span></td>
										<td align='left' class='tdborder'><span>".   $parkingFee ."</span></td>
										<td align='left' class='tdborder'><span>".   $createdBy ."</span></td>
										<td align='left' class='tdborder'><span>".   $newDate ."</span></td>
									</tr>";
						
								}
							} 
						
								 $filedata .= "</table>
							</td>
						</tr>";
					
	
			}
		}	
}			
 $filedata .= "
</table>
<br/><br/>";
}

/* template for all the materials used in project */
if($PROJECTDETAILS->workorder == 'Onderdelen brandveiligheid')
{

	if(count($TotalUSEDMATERIALS) > 0)
	{

	
		$filedata .= "<div style='page-break-before:always;' align='center'>
		 	<table nobr='true' cellpadding='3' cellspacing='0' width='100%' align='center'>
			<tr><td>
			<fieldset style='border-bottom:none; border-left:none; border-right:none; border-top:2px solid red;font-size:25px;color:red;'>
			<legend>[".   $lang['Detail Of Used Material In Project'] ."]</legend></fieldset></td></tr>
			</table>
				<table nobr='true' cellpadding='3' cellspacing='0' width='90%' align='center'>
				
				<tr class='heading'>	
					<td align='left'><span>".   $lang['Material Name']."</span></td>
					<td align='left'><span>".   $lang['Quantity']."</span></td>
					<td align='left'><span>".   $lang['Used In']."</span></td>
				</tr>";
	
	foreach($TotalUSEDMATERIALS as $MATERIALLIST)
	{
		for($i=0;$i<count($MATERIALLIST);$i++)
		{
			$materialName = $MATERIALLIST[$i]->material_name;
			$materialQuantity = $MATERIALLIST[$i]->material_number;
			$usedInTicket = $MATERIALLIST[$i]->projecttaskname;
	
			 $filedata .= "<tr>	
					<td align='left'><span>".   $materialName."</span></td>
					<td align='left'><span>".   $materialQuantity ."</span></td>
					<td align='left'><span>".   $usedInTicket ."</span></td>
			</tr>";

		}
	}	
	 $filedata .= "</table>";
	}
}
 $filedata .= "</div></div>";
 echo $filedata;

$htmlFile = 'temp/'.$html_file;
// $path = 'temp/';

// if ($handle = opendir($path))
// {
    // while (false !== ($file = readdir($handle))) 
	// {
	    // $filelastmodified = filemtime($path . $file);
    //    24 hours in a day * 3600 seconds per hour
        // if((time() - $filelastmodified) > 24*3600)
        // {
           // unlink($path . $file);
        // }

    // }
    // closedir($handle); 
// }
$path = 'temp/';
if ($handle = opendir($path))
{
    while (false !== ($file = readdir($handle))) 
	{
	   // echo $file.'<br/>'; echo $path.'<br/>';
		$filelastmodified = filectime($path . $file);
		// echo $filelastmodified.'<br/>';
			// echo 'time'.$t =time();		echo '<br/>';
		//echo $t =time() - $filelastmodified;
		
        //24 hours in a day * 3600 seconds per hour
        if((time() - $filelastmodified) > 10*60)
        {
			//echo 'deleted'.'<br/>';
           unlink($path . $file);
        }

    }
    closedir($handle); 
}		
			$fh = fopen($htmlFile,'w++') or die("can't open $htmlFile file");

		//$PDFStr = strip_tags($PDFStr,ALLOWED_TAGS);
			fwrite($fh, $filedata) or die("can't write $htmlFile file");
			fclose($fh);
		?>
		<script>
		
		$('document').ready(function(){
	var divhtml = "<div style='z-index: 10001; position: fixed; padding: 0px; margin: 0px; width: 30%; top: 7%; left:77%; text-align: center; color: rgb(0, 0, 0); background-clip: border-box; border-radius: 2px; cursor: wait;' class='blockUI blockMsg blockPage'><div><div class='imageHolder blockMessageContainer' style='text-align: center;'><img src='layouts/vlayout/skins/bluelagoon/images/loading.gif' class='loadinImg alignMiddle'><div class='message'><span>Even geduld, het logboek wordt gegenereerd </span></div></div></div></div>";
	$('input[type="submit"]').css('display','none');
	$('.loading_div').html(divhtml);
		$.ajax({
  method: "POST",
  url: "generate_pdf.php",
  data: { pdf_file: "<?php echo $pdf_file;?>",pdf_html_file: "<?php echo $html_file;?>"}
})
  .done(function( msg ) {
 // $('#submit_btn').enable();
   $('input[type="submit"]').css('display','');
    $('.loading_div').css('display','none');
  });
		})
		
		</script>
		

<?php 
}
else 
{
	echo "<h2 align='center'>Please login first</h2>";
}
/********************************************
Function for generation the stikcers of a fresh image file
Code developed by Nidhishanker Modi
Date: 24 June 2015
*********************************************/
	function IBOKS_PLAN_STICKER($target, $newcopy, $w, $h, $point)
	{
		list($w_orig, $h_orig) = getimagesize($target);
		$scale_ratio = $w_orig / $h_orig;
		if (($w / $h) > $scale_ratio)
		{
			$w = $h * $scale_ratio;
		}
		else
		{ 
			$h = $w / $scale_ratio; 
		} 
		$imgX = intval($point[0])-($w / 2.5);
		$imgY = intval($point[1])-($h / 2.5);

		$img = ""; 
		$img = imagecreatefromjpeg($target); 

		//	Create the zoom_out and cropped image 
		$myImageZoom=imagecreatetruecolor($w*$scale_ratio,$h*$scale_ratio);
		//Fill the two images 
		imagecopyresampled($myImageZoom,$img,0, 0, $imgX, $imgY,$w*$scale_ratio,$h*$scale_ratio,$w,$h);

		$tci = imagecreatetruecolor($w, $h); 
		// imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
		imagecopyresampled($tci, $img, $imgX, $imgY, $imgX, $imgY, $w, $h, $w_orig, $h_orig);
		imagejpeg($myImageZoom, $newcopy, 84); 
	}
?>
