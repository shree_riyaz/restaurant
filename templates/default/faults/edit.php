<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   facility and Services
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
	
$lang = $language->english($lang);
?>

<script language="javascript"> 
$(document).ready(function() {
 $("select.js-example-basic-multiple").select2({
	  tags: true,
	  tokenSeparators: [',', ' '],
	  placeholder: "Please Select",
	  maximumSelectionLength: 10
 });
}); 

function Checkfiles()
{
	var fup = document.getElementById('mini_image');
	var fileName = fup.value;
	var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
	if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png")
	{
		return true;
	}
	else
	{
		alert("Plaese upload image only");
		return false;
	}
}

</script>
	
<form method="post" name="facility_add" id="facility_add" enctype="multipart/form-data">
<center>
	<?php 
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?>

 <table width="60%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody><tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2" style="padding-left: 5px;"><font color="#FFFFFF"><?php echo $lang['Edit Facility/Services']?></font></th>
    </tr>
	<tr>
		<td   colspan="2"  style="font-size:9px"align="right"  class="fontstyle"><?php echo $lang['All fields are mandatory']?></td>
	</tr>
	<?php if ($_SESSION['usertype'] == 'admin') { ?>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Related To']?></label></td>
		<td align="left">
		<div class="col-xs-4">
		
		<select class="form-control" name="company_id">
		<option value="">Please Select</option>
		<?php 
		if($_SESSION['company_id'])	
		{
			$selected = 'selected';
		}
		
		for($i=0;$i<count($company[0]);$i++)
		{
		?>
		<option value="<?php echo $company[0][$i]->company_id;?>" <?php if($Row->company_id == $company[0][$i]->company_id) {?> selected = "selected" <?php } ?>><?php echo $company[0][$i]->company_name;?>
		</option>
		<?php
		}
		?>
		</select>
		</div></td>
	</tr><?php } else { ?>
		<input type="hidden" class="form-control" name="company_id" value="<?php echo $Row->company_id;;?>">
		<?php } ?>
	<tr> 
		<td class="fontstyle" align="right">
		 <div class="form-group" style="width:50% !important;">
            <label for="service_name" class="control-label col-xs-10"><?php echo $lang['Facility/Services Name']?></label></td>
		<td align="left"><div class="col-xs-4">
		   <input type="text" title="Enter Facility/Services Name" class="form-control" id="service_name" name="service_name" value="<?php echo $Row->service_name;?>"> </div>
     </div>
		
	<tr>
		<td class="fontstyle" align="right"><label for="faults" class="control-label col-xs-10"><?php echo $lang['Faults']?> </label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control js-example-basic-multiple" name="faults[]" id="faults" multiple="multiple">
		<?php
			for($i=0;$i<count($faults_row[0]);$i++)
			{ ?>
			
			<option value="<?php echo $faults[0][$i]->fault_id;?>" <?php if($faults[0][$i]->fault_id == $faults_row[0][$i]->fault_id) {?> selected = "selected" <?php } ?>><?php echo $faults[0][$i]->fault_name;?>
			</option>
			
			<?php }
		?>
		
		</select> </div></td>
	</tr>
		
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="description" class="control-label col-xs-10"><?php echo $lang['Description']?></label></td>
		<td align="left">
		<div class="col-xs-4">
		<textarea class="form-control" id="service_description" name="service_description">
		<?php echo preg_replace('/\s+/', ' ',$Row->service_description);?></textarea> </div> </td>
	</tr>
	<tr> 
	<tr>
		<td align="right" class="fontstyle" valign='top'><?php echo $lang['Image']?></td>
		<td align="left" colspan="3">
		<div class="col-xs-4"><input  type="file"  name="image" class="form-control"></div>
		<div class="col-xs-4" style="float:right; border:solid 2px #999;  margin-bottom:3px;">
		<img src="<?php echo IMAGEURL."uploads/".$Row->image;?>" title="image"   height="200px" width="200px"/></div>
		<input type="hidden" name="img_name" value="<?php echo $Row->image;?> "  />	
		</td>
		
	</tr>
	
	<tr class="alt">
		<td colspan=2 style="text-align: center;" align="left">
		<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		
		<button type="submit" class="btn btn-primary" name="update"><?php echo $lang['Update']?></button>
		<input type="button" value="<?php echo $lang['Reset']?>" onClick="this.form.reset()" class="btn btn-primary"/>
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=facilityandServices');?>'"><?php echo $lang['Back']?></button>
     </div>
		</td>
	</tr>
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b></div>

	
</form>

</center>
</body>

</html>
