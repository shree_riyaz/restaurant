<?php
	/*======================================
	Developer	-	JAishree Sahal
	Module      -   Faults
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/

?>

<script>

function Check()
{
	if(document.getElementById('keyword').value=='')
	{
		alert('Please enter any value for search.');
		return false;
	}
	else
	{
		return true;
	}
}
function Reset() {
    document.getElementById("frmlist").reset();
}
</script>

<br />
<?php
$lang = $language->english($lang);
?>
<form method="post" name="frmlist" id="frmlist" class="form-horizontal">
<?php
	if (isset($_SESSION['ActionAccess']))
	{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
				echo $_SESSION['ActionAccess'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['ActionAccess']);

	}
	if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
				echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="successmsg">';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);

			}
			?><center>


	<table class="table" style="width:50% !important;">
    <tbody>
	<tr valign="middle" align="center">
      <th height="30" class="thColor" colspan="2" style="padding-left: 5px;">
	  <font color="#FFFFFF"><?php echo $lang['Search Fault']?></font></th>
    </tr></table>
	 <div class="form-group" style="width:50% !important;">
            <label for="username" class="control-label col-xs-4"><?php echo $lang['Search Fault']?></label>
		<div class="col-xs-7">
		   <input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo (isset($frmdata['keyword'])?$frmdata['keyword']:'');?>" onbuler="return  KeywordSearch()" > </div>
     </div>
	 <div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">

		<button type="submit" class="btn btn-primary" name="search" onclick="return Check()"><?php echo ['Search']?></button>
		<button type="submit" class="btn btn-primary" name="clearsearch"><?php echo ['Reset']?></button>
     </div>

	<br/>		<br/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"  >

		<tr>
  		<td align="right" height='12'>&nbsp;
		<a title="Add New Facility/Services" style=" font-family:Arial, Helvetica, sans-serif;cursor:hand;font-size:14px" href="index.php?mod=faults&do=add ">
		<?php echo $lang['Add New Fault']?>
        </a>
		</td>
  		</tr>
</table>

<?php
		$srNo=$frmdata['from'];
		$count=count($Row);
		?>
		<table align="center" cellpadding="0"  cellspacing="0" border="0" class="table table-striped" style="font-size:14px;" width="100%">

		</tr>
			<tr class="trwhite">
			<td colspan="2" align='left'>
				<?php
					if($totalCount > 0)
					   print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
					else
					   print "Showing Results:0-0 of 0";
					?>
					</td>
					<td colspan="7"  align="right" ><?php echo getPageRecords();?></td>
				</tr>

		</tr>
	<tr class="tblheading">
		<th class="anth"height="36" ><?php echo ['S.No.']?></th>

		<th  align="center" class="anth">
		<a  style="color:#ffffff; cursor:pointer" onClick="OrderPage('service_name');"><?php echo $lang['Fault Name']?>
		<?php if($frmdata['orderby']=='service_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='service_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>

		<th  align="center" class="anth">
		<a   style="color:#ffffff;  cursor:pointer" onClick=
		"OrderPage('service_description');"><?php echo $lang['Facility/Services Name']?>
		<?php if($frmdata['orderby']=='service_description') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='service_description desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a   style="color:#ffffff;  cursor:pointer" onClick=
		"OrderPage('service_name');"><?php echo $lang['Image']?>
		<?php if($frmdata['orderby']=='image') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='image desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<!--<th class="anth">View</th>-->
		<th class="anth"><?php echo $lang['Edit / Delete']?></th>
	</tr>
<?php
if($Row)
{
	for($counter=0;$counter<$count;$counter++)
	{
		$srNo++;
		if(($counter%2)==0)
		{
			$trClass="tdbggrey";
		}
		else
		{
			$trClass="tdbgwhite";
		}
		$confirmDelete = 'Do you really want to delete this Facility/Services ?';
?>

	<tr>
		<td align='center'><?php echo $srNo; ?></td>
		<td align='center'><?php echo ucfirst($Row[$counter]->fault_name); ?>&nbsp;		 		</td>
		<td align='center'><?php echo ucfirst($Row[$counter]->service_name); ?></td>
		<td align='center'><img src="<?php echo IMAGEURL."uploads/".$Row[$counter]->image ?>" height="50px" width="50px;"/></td>
		<td align='center'>
			<a class="fontstyle"
				 href='<?php print
					CreateURL('index.php','mod=fault&do=edit&id='.$Row[$counter]->fault_id);?>' 				title="Edit" >
						<img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 />
			</a>
			<a title="Delete"
				href='<?php print
				CreateURL('index.php','mod=fault&do=del&id='.$Row[$counter]->fault_id);?>'
				onclick="return confirm('<?php echo $confirmDelete ?>')">
				<img src="<?php print IMAGEURL ?>/b_drop.png">
			</a>
		</td>



</tr>
	<?php
		$sno++;
	}
?>
</table>
   <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
		 <tr>
			  <td align="center" colspan="12">
				  <?php
					 PaginationDisplay($totalCount);
				   ?>
			  </td>
		</tr>
	 </table>
	</div>
<?php
}
else
{

$frmdata['message']="Sorry ! Facility/Services not found for the selected criteria";
		ShowMessage();
?>
<style> .tblheading,.trwhite{ display:none; }

</style>
<?php

}?>


      <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
	  <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'service_name';?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" /></form>

</center>