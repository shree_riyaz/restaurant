<?php
	/*======================================
	Developer	-	Neha Pareek
	Module      -   Device Locations
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
//include_once("lib/language.php");
$language = new Language();
$lang = $language->english('eng');
?>	


<br />
<br />	
<form method="post" name="location_add" id="company_add" enctype="multipart/form-data">

<center>
	<?php 
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?>

 <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody><tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2"  style="padding-left:5px;" ><font color="#FFFFFF"><?php echo $lang['Edit Device Location']?></font></th>
    </tr>
	
	<tr> 
		<td class="fontstyle" align="right">
		 <div class="form-group" style="width:72% !important;">
            <label for="location_name" class="control-label col-xs-10"><?php echo $lang['Location Name'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4">
		   <input type="text" title="Enter Device Location Name" style="width:185px;" class="form-control" id="location_name" name="location_name" value="<?php echo $Row->location_name;?>"  > </div>
    </tr>
<tr> 
		<td class="fontstyle" align="right">
		 <div class="form-group" style="width:85% !important;">
            <label for="location_name" class="control-label col-xs-12"><?php echo $lang['Location Description'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4">
        <textarea name="location_description" title="Enter Device Location" class="form-control"><?php echo $Row->location_description;?></textarea>
		  </div>
    </tr>	
    <tr> 
		<td align="right" class="fontstyle" width="30%"><label for="active" class="control-label col-xs-10"><?php echo $lang['Active'].MANDATORYMARK?></label></td>
		
		<td align="left"><div class="col-xs-6">
			<input type="radio" name="is_active" id="is_activeY" value="Y" <?php if($Row->is_active=='Y') {?> checked <?php } ?> /><?php echo $lang['Active']?>  &nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="is_active" id="is_activeN" value="N" <?php if($Row->is_active=='N') {?> checked <?php } ?>/><?php echo $lang['In-Active']?>
			</div>
		</td>
	</tr>
	<tr class="alt">
		<td colspan=2 style="text-align: center;" align="left">
		<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		
		<button type="submit" class="btn btn-primary" name="update"><?php echo $lang['Update']?></button>
		<!--<button type="reset" class="btn btn-primary" name="Reset"><?php echo $lang['Reset']?></button>-->
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=device_locations');?>'"><?php echo $lang['Back']?></button>
     </div>
		</td>
	</tr>
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b>

	
</form>

</center>
</body>

</html>
