<?php
	/*======================================
	Developer	-	Neha Pareek
	Module      -   Device Locations
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
//include_once("lib/language.php");
$language = new Language();
$lang = $language->english('eng');
?>	

    <section>

                <div class="col-sm-9 drop-shadow nopadding">
                    <form method="post" name="location_add" id="company_add" class="form-horizontal" enctype="multipart/form-data">

                        <?php
                        if(isset($_SESSION['error']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                    <div class="alert alert-danger alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['error'];
                            echo '</div></td></tr></tbody></table>';
                            unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success']))
                        {
                            echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $_SESSION['success'];
                            echo '</div></td></tr></tbody></table>';
                            unset($_SESSION['success']);
                        }
                        ?>
                    <div class="user-heading text-left">
                        <span><?php echo $lang['Update Device Location'] ?></span>
                        <?php
                        include_once 'user_profile.php';
                        ?>
                    </div>
                    <div class="userbg">

                        <div id="users" class="">
                            <h4 class="update-user"><?php echo $lang['Update Device Location'] ?></h4>
                        </div>
                        <div class="plan-category user-page-form">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-right">
                                        <sup>*</sup><span class="validation"><?php echo $lang['All Fields are Mandatory'] ?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3"><?php echo $lang['Location Name'] ?>
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <input type="text" title="Enter Device Location Name" style="width:185px;" class="form-control" id="location_name" name="location_name" value="<?php echo $Row->location_name;?>"  >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3"><?php echo $lang['Location Description'] ?>
                                    <sup>*</sup></label>
                                <div class="col-sm-9">
                                    <textarea name="location_description" title="Enter Device Location" class="form-control"><?php echo $Row->location_description;?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3"><?php echo $lang['Active']?>
                                    <sup>*</sup></label>
                                <div class="col-sm-3">
                                    <input type="radio" name="is_active" id="is_activeY" value="Y" <?php if($Row->is_active=='Y') {?> checked <?php } ?> /><?php echo $lang['Active']?>  &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="is_active" id="is_activeN" value="N" <?php if($Row->is_active=='N') {?> checked <?php } ?>/><?php echo $lang['In-Active']?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" id="update" name="update" class="btn btn-danger add-company pull-right" value="Update"><?php echo $lang['Update']?></button>

                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
    </section>
