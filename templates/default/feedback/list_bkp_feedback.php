<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   Feedback
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/
?>

<script>

function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');

	if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i);
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }

function showpassdiv(id){
$("#password_"+id).css("display", "block");
}
function Check()
{
	if(document.getElementById('keyword').value=='')
	{
		alert('Please enter any value for search.');
		return false;
	}
	else
	{
		return true;
	}
}
function Clear()
{
	document.getElementById('keyword').value='';
	location.href="index.php?mod=feedback";
	return false;
}
</script>

<br />
<?php
$lang = $language->english($lang);
?>
<form method="post" name="frmlist" id="frmlist" >
<?php
	if (isset($_SESSION['ActionAccess']))
	{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
				echo $_SESSION['ActionAccess'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['ActionAccess']);

	}
	if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?>
<center>

<div style=" border: 1px #ddd solid; width:50% !important; display: table; text-align : center;">
<table class="table" style="width:100% !important;">
    <tbody>
	<tr valign="middle" align="center">
      <th height="30" class="thColor" colspan="2" style="padding-left: 5px;"><font color="#FFFFFF"><?php echo $lang['Search Feedback']?></font></th>
    </tr></table>
	 <div class="form-group" style="width:100% !important;">
            <label for="username" class="control-label col-xs-4">
			<?php echo $lang['Search Feedback']?></label>
		<!--<div class="col-xs-7">-->
		   <input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo (isset($frmdata['keyword'])?$frmdata['keyword']:'');?>" onbuler="return  KeywordSearch()" >
		   <span style="font-size:11px">&nbsp;&nbsp*Search By Feedback ,Device Name,Rating,Device Status,Status,Assigned By,Assigned To,Creation Date, Modified Date</span>
		   <!--</div>-->
     </div>

	 <!--<br/><br/><br/>-->
	 <div class="col-xs-offset-2 col-xs-10" style="width:100% !important; margin-left: 0px; padding-bottom : 10px;">

		<button type="submit" class="btn btn-primary" name="search"  onclick="return Check()"><?php echo $lang['Search']?></button>
		<button type="reset" class="btn btn-primary" name="clearsearch" id="clear" onclick="return Clear()"><?php echo $lang['Reset']?></button>
     </div>
</div>
	<br/>	<br/>
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center"  >

		<tr>
  		<td align="right" height='12'>&nbsp;
  		<?php
		if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' )
		{
		?>
		<a title="Add New User" style=" font-family:Arial, Helvetica, sans-serif;cursor:hand;font-size:14px" href="index.php?mod=feedback&do=add "><?php echo $lang['Add New Feedback']?>
        </a>
        <?php
		}
		?>
		</td>
  		</tr>
</table>
<?php
   // echo '<pre>';print_r($Row);
$srNo=$frmdata['from'];
$count=count($Row);
?>
<table width="90%" align="center" cellpadding="0"  cellspacing="0" border="0" class="table table-striped" >
		<tr class="trwhite">

			<?php
			if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' )
			{
			?>
			<td align="left" style="padding-left:3px;" >
			<select name="actions" onchange="this.form.submit()" class="form-control">
			<option value="">Select Action</option>
			<option value="Delete">Delete</option>
			<option value="Active">Active</option>
			<option value="Inactive">In-Active</option>
			</select>
		   </td>
		   <?php
			 }
			?>
			<td colspan="9" align='right' style="padding-right:20px;">
			<?php
			if($totalCount > 0)
			   print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
			else
			   print "Showing Results:0-0 of 0";
			?>
			</td>
			<td align="right" width="120" colspan="3" ><span style="float: left; margin-top: 9px;" align : "center">&nbsp;&nbsp;Show :</span><?php echo getPageRecords();?></td>

					<!--</tr>-->
		</tr>
	<tr class="tblheading">
		<th class="anth"height="36" >
		<?php
		if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' )
		{
		?>
		<input type="checkbox" name="chkAll[]" id="chkAll"  onchange="checkAll(this)"/>&nbsp;
		<?php } ?><?php echo $lang['S.No.']?>
		</th>
		<th  align="center" class="anth">
		<a  style="color:#FFF;cursor:pointer;" onClick="OrderPage('faults.fault_name');"><?php echo $lang['Feedback']?>
		<?php if($frmdata['orderby']=='faults.fault_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='faults.fault_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('f.is_active');"><?php echo $lang['Status']?>
		<?php if($frmdata['orderby']=='f.is_active') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='f.is_active desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a  style="color:#FFF;cursor:pointer;" onClick="OrderPage('d.device_name');"><?php echo $lang['Device']?>
		<?php if($frmdata['orderby']=='d.device_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='d.device_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a  style="color:#FFF;cursor:pointer;" onClick="OrderPage('f.rating');"><?php echo $lang['Rating']?>
		<?php if($frmdata['orderby']=='f.rating') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='f.rating desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a  style="color:#FFF;cursor:pointer;" onClick="OrderPage('d.device_status');"><?php echo $lang['Device Status']?>
		<?php if($frmdata['orderby']=='d.device_status') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='d.device_status desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a  style="color:#FFF;cursor:pointer;" onClick="OrderPage('f.feedback_status');"><?php echo $lang['Status']?>
		<?php if($frmdata['orderby']=='f.feedback_status') {print '<img src="'.IMAGEURL.'/desc.gif">';} elseif($frmdata['orderby']=='f.feedback_status desc'){print '<img src="'.IMAGEURL.'/asc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a  style="color:#FFF;cursor:pointer;" onClick="OrderPage('u.first_name');"><?php echo $lang['Assigned By']?>
		<?php if($frmdata['orderby']=='u.first_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='u.first_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
        <th  align="center" class="anth">
		<a  style="color:#FFF;cursor:pointer;" onClick="OrderPage('u1.first_name');"><?php echo $lang['Assigned To']?>
		<?php if($frmdata['orderby']=='u1.first_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='u1.first_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a  style="color:#FFF;cursor:pointer;" onClick="OrderPage('f.creation_date');"><?php echo $lang['Creation Date']?>
		<?php if($frmdata['orderby']=='f.creation_date') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='f.creation_date desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a  style="color:#FFF;cursor:pointer;" onClick="OrderPage('f.modified_date');"><?php echo $lang['Modified Date']?>
		<?php if($frmdata['orderby']=='f.modified_date') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='f.modified_date desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>

		<th class="anth"><?php if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin') echo $lang['Edit / Delete'] ; else echo $lang['Edit'];?></th>
	</tr>
<?php
if($Row)
{

    $objpassEncDec = new passEncDec;
	for($counter=0;$counter<$count;$counter++)
	{
				$srNo++;
				if(($counter%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				}



          $confirmDelete = 'Do you really want to delete this Feedback ?';
		  $obj = new passEncDec;
		  ?>

	<tr>
		<td align="center">
		<?php
		if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' )
		{
		?>
		<input type="checkbox" name="chkbox[]" id="chkbox" value="<?php echo $Row[$counter]->feedback_id;?>"/>&nbsp;&nbsp;
		<?php
		}
		echo $srNo;
		?>
		</td>

		<td align='center'><?php echo ucwords($Row[$counter]->fault_name); ?>&nbsp;</td>
		<td align='center'><?php if($Row[$counter]->is_active == 'Y')echo ucfirst("active"); else echo ucfirst("in-Active");?>&nbsp;</td>
		<td align='center'><?php echo ucfirst($Row[$counter]->device_name); ?>&nbsp;</td>
		<td align='center'><?php echo ucfirst($Row[$counter]->rating); ?>&nbsp;</td>
		<td align='center'><?php echo ucfirst($Row[$counter]->device_status); ?>&nbsp;</td>
		<td align='center'><?php if($Row[$counter]->feedback_status =='1') { echo 'Completed'; } else { echo 'Pending';} ?></td>
		<td align='center'><?php echo ucwords($Row[$counter]->first_name.' '.$Row[$counter]->last_name); ?></td>
        <td align='center'><?php echo ucwords($Row[$counter]->assignF.' '.$Row[$counter]->assignL); ?></td>
		<td align='center'><?php if($Row[$counter]->creation_date) {echo date('m/d/Y',strtotime($Row[$counter]->creation_date)); }
			else echo '-' ; ?></td>
		<td align='center'><?php if($Row[$counter]->modified_date) {echo date('m/d/Y',strtotime($Row[$counter]->modified_date)); }
			else echo '-' ; ?></td>
		<td align='center'>
		<?php
		if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin')
		{
		?>
		<a class="fontstyle" href='<?php print CreateURL('index.php','mod=feedback&do=edit&id='.$Row[$counter]->feedback_id);?>' title="Edit" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 /></a>
		<a title="Delete"  href='<?php print CreateURL('index.php','mod=feedback&do=del&id='.$Row[$counter]->feedback_id);?>' onclick="return confirm('<?php echo $confirmDelete ?>')"><img src="<?php print IMAGEURL ?>/b_drop.png"></a>
		<?php }
		else
		{
		?>
		<a class="fontstyle" href='<?php print CreateURL('index.php','mod=feedback&do=edit&id='.$Row[$counter]->feedback_id);?>' title="Edit" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 /></a>
		<?php } ?>
		</td>

	</tr>
	<?php
		$sno++;
	}
?>
</table>
   <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
		 <tr>
			  <td align="center" colspan="12">
				  <?php
					PaginationDisplay($totalCount);
				   ?>
			  </td>
		</tr>
	 </table>
	<!--</div>-->
<?php
}
elseif($_SESSION['keywords'] == 'Y')
{
	$frmdata['message']="Sorry ! Feedback not found for the selected criteria";
	unset ($_SESSION['keywords']);
	ShowMessage();
}
else
{
	$frmdata['message']="Sorry ! No feedback. Please add feedback first.";
	ShowMessage();
?>

<style> .tblheading,.trwhite{ display:none; }

</style>
<?php

}?>


      <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
	  <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'f.feedback_id';?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" /></form>

</center>