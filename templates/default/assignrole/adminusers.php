<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Admin Users</title>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<script>
function CheckallDelete()
{
			count = document.getElementsByName('delchk[]').length;
			
			for(counter=0;counter<count;counter++)
			{
					document.getElementById('delchk'+counter).checked = true;
			}
		
}
function UnCheckallDelete()
{
			count = document.getElementsByName('delchk[]').length;
			
			for(counter=0;counter<count;counter++)
			{
					document.getElementById('delchk'+counter).checked = false;
			}
		
}
function ifdeleteVideo(val)
{
	 if(confirm("Do you really want to delete User(s)?"))
	{
		document.getElementById('id').value=val;
		document.forms[0].submit();
		return true;
	}
	else
	{
		return false;
	}
}

function Check(RecordCount,Field,Type)
	{
	 
	var Flag = 0; //keeps track that atleast one record is selected to change the status
	 var Counter = 0; 
	 var Field = document.getElementsByName('delchk[]');
	 if(RecordCount == 1)
	 {
	     if(Field[Counter].checked == true)
		 {
		    Flag = 1;
		  }
		 else
		 {

		    Flag = 0;
		  } 
	 }	  	
	 else
	 {
	  	for (Counter=0; Counter < RecordCount; Counter++) 
		{
		  if(Field[Counter].checked == true)
			{
			   Flag = 1;
			   break;
			 }    
		   else
		   {
			 Flag = 0;
			}
		}  	
	}
		
     if(Flag == 0)
	 {

		if(Type == "deletetrade")
		{
			alert("Please select atleast one User to delete.");
		}
		return false;
	 }
	 else
	 	return true;	
}

</script>
<body >

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<fieldset class="rounded"><legend>Admin Users</legend>
			<?php 
			if (isset($_SESSION['ActionAccess']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['ActionAccess'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['ActionAccess']);
			}
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>	
		<table width="40%" border="0" cellspacing="1"  style="margin-top:12px" cellpadding="0" align="center" class="tableccss">
		<tr >
		  <th height="30" colspan="7" align="center" style="font-size:14px">Admin Users</th>
		</tr> 
		<tr class="trwhite">			
			<td align="right" class="fontstyle">Keyword:</td>
			<td align="left"><input name="keyword" type="text" size="30" class="rounded" id="keyword" maxlength="20"   value="<?php echo $frmdata['keyword']; ?>" /></td>
		</tr>
	 	<tr class="trwhite">	 
	 		<td height="36" colspan="4" align="center">
				<input class="buttons rounded" type="submit" name="search"   value="Search"  title="Click to search">
				&nbsp;&nbsp;
		        <input class="buttons rounded" type="submit" name="clearsearch" value="Clear Search"   title="Click to clear">
			</td>
		</tr>		
	  </table>
	<br>	
	  <?php 
		if($userlist)
		{		   
		 $srNo=$frmdata['from'];
		 $count=count($userlist);	 	   
		?>	  
	  <table width="80%" align="center" cellpadding="1"  cellspacing="1" border="0" bgcolor="#e1e1e1" class="tablecss" style="margin-top:15px;">
		<tr class="trwhite"> 
			<td colspan="10">
				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
						<td width="454" align="left" valign="top">
						<?php	
							print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
						?>
						</td>		
						<td width="205" align="right" valign="top"><?php echo getPageRecords();?></td>
					</tr>
				</table>
			 </td>
		</tr> 
		</tr>
		<tr class="tblheading">
		  <td width="33" align="center" height="36" >S.No</td>
		  <td align="center">Army No.</td>
		  <td align="center">
		  	<a  style="color:#AF5403; text-decoration:underline;cursor:pointer" onClick="OrderPage('name');">Name
			<?php if($frmdata['orderby']=='name') {print '<img src="'.ROOTURL.'/images/asc.gif">';} elseif($frmdata['orderby']=='name desc'){print '<img src="'.ROOTURL.'/images/desc.gif">';}?>
			</a>
		  </td>
		  <td align="center">Role</td>
		  <td align="center">From Date</td>
		  <td align="center">To Date</td>	  
		  <td width="100" align="center" >Action</td>
		  <td width="20%"><a href="javascript:CheckallDelete()"  style="color:#AF5403;" >Select All</a> / <a href="javascript:UnCheckallDelete()" style="color:#AF5403;" >Unselect All</a></td>
		</tr>
		
		<?php 	
			for($counter=0;$counter<$count;$counter++)
			{
				$srNo++;
				if(($counter%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				}
				if ($userlist[$counter]->name == '')
				{
					$userlist[$counter]->name = $userlist[$counter]->first_name;
				}
			
		?>
		<tr class='<?= $trClass; ?>'>
		   <td align="center" ><?php  print $srNo ?></td>		   
		   <td align="center" class="fontstyle" <?php echo $style ?>>&nbsp;<?php echo highlightSubString($frmdata['keyword'],$userlist[$counter]->candidate_id) ?></td>	
		   <td align="center" class="fontstyle" <?php echo $style ?>>&nbsp;<?php echo highlightSubString($frmdata['keyword'],$userlist[$counter]->name) ?></td>	
		   <td align="center" class="fontstyle" <?php echo $style ?>>&nbsp;<?php echo highlightSubString($frmdata['keyword'],$userlist[$counter]->roleName) ?></td>	
		   <td align="center" class="fontstyle" <?php echo $style ?>>&nbsp;<?php if ($userlist[$counter]->fromDate != '' && $userlist[$counter]->fromDate != '0000-00-00 00:00:00') { echo date('d-m-Y h:i:s',strtotime($userlist[$counter]->fromDate));} else { echo "n/a"; } ?></td>		 
		   <td align="center" class="fontstyle" <?php echo $style ?>>&nbsp;<?php if ($userlist[$counter]->toDate != '' && $userlist[$counter]->toDate != '0000-00-00 00:00:00') { echo date('d-m-Y h:i:s',strtotime($userlist[$counter]->toDate));} else { echo "n/a"; } ?></td>		 
		   <td align="center" <?php echo $style ?>>
		  		<a title = "Edit" class="fontstyle" href='<?php print CreateURL('index.php','mod=assignrole&do=editassignrole&nameID='.$userlist[$counter]->id);?>' ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 /></a>
				<a title="Delete"  href='<?php print CreateURL('index.php','mod=assignrole&do=del&nameID='.$userlist[$counter]->id);?>' onclick="return confirm('Do you really want to delete this user from admin user list?')"><img src="<?php print IMAGEURL ?>/b_drop.png"></a>
		   </td>
		   <td>
		  		
								 <input name="delchk[]" type="checkbox" id="delchk<?php echo $counter;?>" value="<?php print $userlist[$counter]->id;?>" />
				
		   </td>
		</tr>		
		<?php
			} 
		?>
	</table>
	<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">		
		 <tr>
			  <td align="center" colspan="12">
				  <?php
					PaginationDisplay($totalCount);
				   ?>
			  </td>
		</tr>
	 </table>
	 <table width="100%">
				<tr>
					<td align="center" >
			               <input class="input" type="submit" value="Delete Selected" name="deletetrade" onClick="if(Check('<?php print $count;?>',document.frmlist.delchk,'deletetrade')){ return ifdeleteVideo('<?php echo $count;?>') }else {return false;} "></td>
				</tr>
				</table>
	
	  <?php
	  }
	  else
	  { 
		$frmdata['message']['text']="Sorry ! User not found for the selected criteria";
		ShowMessage();
	   }
	  
	  ?>
	  <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
	  <input name="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" />
	</form>	
		</div><!--Div Contents closed-->
				</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	</table>	
</div><!--Outer wrapper closed--> 
</body>
</html>
