<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Assign Role</title>

</head>
<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<body>
<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
				<?php include_once(CURRENTTEMP."/"."navigation.php"); ?>
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<fieldset class="rounded"><legend>Edit User Role</legend>
			<?php 
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>
			<table width="55%" border="0" cellspacing="1" style="margin-top:10px"  cellpadding="3" align="center" class="tableccss">
			<tr > 
				<th height="30" style=" font-family:Arial, Helvetica, sans-serif;font-size:14px" colspan="8" >Edit User Role</th>
			</tr> 
			<tr>
				<td   colspan="4"  style="font-size:9px"align="right"  class="fontstyle">Fields marked with ( <span style="color:red">*</span> ) are mandatory</td>
			</tr>	   
			
			<tr>
				<td align="right" class="fontstyle">Role:</td>	
				<td align="left" >
				<select name="role_id" id="role_id" class="rounded">
				<option value="">Please Select Role</option>
				<?php
				if ($roles)
				{
					$count = count($roles);
					
					for($counter = 0; $counter < $count; $counter++)
					{
						$selected ='';
						if ($users->role_id == $roles[$counter]->roleID)
						{
							$selected = ' selected';
						}
						echo '<option value="'.$roles[$counter]->roleID.'" '.$selected.'>'.$roles[$counter]->roleName.'</option>';
					}
				}	
				?>
				</select><?php echo MANMES?></td>
			</tr> 
			<tr>
				<td align="right" class="fontstyle">User:</td>
				<td align="left">
				<select name="candidate_id" id="candidate_id">				
				<?php 
					if ($users)
					{
						echo '<option value="'.$users->id.'"  >'.$users->candidate_id.' - '.$users->first_name.' '.$users->last_name.'</option>';
					}
				?>
				</select> <?php echo MANMES?>
				</td>
			</tr>
			<tr>
				<td>
					<div align="right">Date:</div>
				</td>
				<td>From: <input type="text" class="rounded" size="35" maxlength="35" value="<?php if ($users->fromDate != '' && $users->fromDate != '0000-00-00 00:00:00') {  echo date('m/d/Y h:i:s', strtotime($users->fromDate)); }?>" name="fromDate" id="demo1">
				&nbsp;<a href="javascript:NewCal('demo1','mmddyyyy', 1, 12)"><img width="16" height="16" border="0" alt="Pick a date" src="<?php echo IMAGEURL ?>/cal.gif"></a></td>
			</tr>
			<tr>
				<td>
					<div align="right"></div>
				</td>
				<td>To:&nbsp;&nbsp;&nbsp; <input type="text" class="rounded" size="35" maxlength="35" value="<?php if ($users->toDate != '' && $users->toDate != '0000-00-00 00:00:00') { echo date('m/d/Y h:i:s', strtotime($users->toDate)); }?>" name="toDate" id="demo2">
				&nbsp;<a href="javascript:NewCal('demo2','mmddyyyy', 1, 12)"><img width="16" height="16" border="0" alt="Pick a date" src="<?php echo IMAGEURL ?>/cal.gif"></a></td>
			</tr>				
			<tr>
				<td colspan="6"><div align="center" style="margin-top:10px">
				<input type="submit" name="assignrole"   id="assignrole" value="Update User Role" class="buttons rounded"  title="Click to update">
				  &nbsp;&nbsp;
				<input type="button" name="fileback" id="fileback" class="buttons rounded"  value="Back" onClick="location.href='<?php print CreateURL('index.php','mod=assignrole&do=adminusers');?>'">
				<br>
				
				</div>
				</td>
			</tr>
		</table>
		<br>
		</form>	
		</div><!--Div Contents closed-->
				</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	</table>	
</div><!--Outer wrapper closed--> 
</body>
</html>
<script>
function showhidePer(module, id)
{
	div = 'div:'+id;
	//alert(div);
	if (module.checked)
	{	
		document.getElementById(div).style.display='';
	}
	else
	{
		document.getElementById(div).style.display='none';
	}	
}
</script>