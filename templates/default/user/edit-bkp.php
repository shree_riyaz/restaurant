<script>
function deleteConfirm()
{
	if(confirm("Are you really want to delete ? "))
		return true;
	else
		return false;
}
function Clear()
{
	document.getElementById('company_id').value='';
	document.getElementById('role_id').value='';
	document.getElementById('first_name').value='';
	document.getElementById('last_name').value='';
	document.getElementById('user_email').value='';
	document.getElementById('confirm_password').value='';
	document.getElementById('password').value='';
	document.getElementById('user_phone').value='';
	document.getElementById('is_activeN').value='';
	document.getElementById('is_activeY').value='';

	return false;
}
</script>

<center>
<br />
		

<br />	
<form method="post" name="user_edit" id="user_edit" enctype="multipart/form-data">
<center>
	<?php 
		
		print_r($Row);
			$lang = $language->english($lang);
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			
			?>

 <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody><tr valign="middle" align="center"> 
	<!--<?php echo "<pre>"; print_r($supervisor[0]);?> 
		<?php echo "<pre>"; print_r($role[0]);?>		-->
      <th height="30" class="thColor" colspan="2" style="padding-left: 5px;"><font color="#FFFFFF"><?php echo $lang['Update User']?></font></th>
    </tr>
	<tr>
		<td colspan="2"  style="font-size:9px" align="right" class="fontstyle"><?php echo $lang['All fields are mandatory'];?></td>
	</tr>
	<?php if ($_SESSION['usertype'] == 'super_admin') { ?>
		<tr> 
		<td align="right" class="fontstyle" width="30%"><label for="username" class="control-label col-xs-10"><?php echo $lang['Related To'] .MANDATORYMARK ?></label></td>
		<td align="left" ><div class="col-xs-4">
		<select name="company_id" class="form-control" id="company_id" disabled="disabled">
		<option value="">Please Select</option>
		<?php 		
		for($i=0;$i<count($company[0]);$i++)
		{ ?>
		<option value="<?php echo $company[0][$i]->company_id ?>" <?php if($Row[0][0]->company_id == $company[0][$i]->company_id){?> selected = "selected"<?php }?>><?php echo $company[0][$i]->company_name ?></option>
		<?php 
		}
		?>
</select>	</div>	</td>
	</tr> <?php } ?>
		<!-- Added By : Neha Pareek, Dated : 4 Nov 2015 -->
		<?php 
		if($_SESSION['usertype'] == 'super_admin') 
		{ ?>
        <tr> 
		<td align="right" class="fontstyle" width="30%"><label for="username" class="control-label col-xs-10"><?php echo $lang['Role'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
       	<select class="form-control" name="role_id" disabled = "disabled">
		<option value="" >Please Select</option>
		<?php 
		for($i=0;$i<count($role[0]);$i++)
		{ ?>
		<option value="<?php echo $role[0][$i]->role_id; ?>" <?php if($Row[0][0]->role_id == $role[0][$i]->role_id){?> selected = "selected" <?php }?>><?php echo $role[0][$i]->role_name;?></option>
		<?php 
		}
		?>
		</select>
		<?php 
		}
		//else
		//{
		?>
		<!--<input type="hidden" class="form-control" name="role_id" value="<?php echo $Row[0][0]->role_id;?>">-->
		<?php 
		//} 
		?>
		<input type="hidden" class="form-control" name="role_id" value="<?php echo $Row[0][0]->role_id;?>">
		</div>
		</td>
	</tr>
	
	 <?php 
		if(($Row[0][0]->role_id ==2) && ($_SESSION['usertype'] == 'company_admin')) 
		{ ?>
        <tr> 
		<td align="right" class="fontstyle" width="30%"><label for="username" class="control-label col-xs-10"><?php echo $lang['Role'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4">
		<select class="form-control" name="role_id" >
		<option value="" >Please Select</option>
		<?php 
		
		/* for($i=0;$i<count($roles[0]);$i++)
		{ ?>
		<option value="<?php echo $roles[0][$i]->role_id; ?>" <?php if($Row[0][0]->role_id == $roles[0][$i]->role_id){?> selected = "selected" <?php }?>><?php echo $roles[0][$i]->role_name; */
		
		//replace all role in loop by roles. Edited by Neha Pareek, Dated : 04nov15
		for($i=0;$i<count($roles[0]);$i++)
		{ 
		?>
		<option value="<?php echo $roles[0][$i]->role_id; ?>" <?php if($Row[0][0]->role_id == $roles[0][$i]->role_id){?> selected = "selected" <?php }?>><?php echo $roles[0][$i]->role_name;?></option>
		<?php 
		}
		?>
		</select>
		<?php  
		}
		//else
		//{?>
		<!--<input type="hidden" class="form-control" name="role_id" value="<?php echo $Row[0][0]->role_id;?>">-->
		<?php //} ?>
		<input type="hidden" class="form-control" name="role_id" value="<?php echo $Row[0][0]->role_id;?>">
		</div>
		</td>
	</tr> 
	<tr> 
		<td align="right" class="fontstyle" width="30%"><label for="username" class="control-label col-xs-10"><?php echo $lang['First Name'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4"><input title="Enter User First Name" class="form-control" type="text"
			name="first_name" id="first_name" size=25 value="<?php if($Row[0][0]->first_name) echo $Row[0][0]->first_name; else echo $_POST['first_name'];?>"></div>  </td>
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"><label for="username" class="control-label col-xs-10"><?php echo $lang['Last Name'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4"><input title="Enter User Last Name" class="form-control" type="text"
			name="last_name" id="last_name" size=25 value="<?php if($Row[0][0]->last_name)echo $Row[0][0]->last_name; else echo $_POST['last_name']?>"></div>  </td>
	</tr>
	<tr>
			<td class="fontstyle" align="right"><label for="username" class="control-label col-xs-10"><?php echo $lang['Email Id'].MANDATORYMARK ?></label></td>
			<td align="left"><div class="col-xs-4"><input title="Enter Email" class="form-control" type="text"
			name="user_email" id="user_email" size=25 value="<?php if($Row[0][0]->user_email)echo $Row[0][0]->user_email; else echo $_POST['user_email']; ?>"></div></td>
	
	</tr>
	<tr>
			<td class="fontstyle" align="right"><label for="username" class="control-label col-xs-10"><?php echo $lang['Password'].MANDATORYMARK ?></label></td>
			<td align="left"><div class="col-xs-4"><input title="Enter Password" class="form-control" type="password"
			name="password" id="password" size=25 value="">
			<?php echo $lang['*Enter password if you want to change password.']?></div></td>
	
	</tr>
	<tr>
			<td class="fontstyle" align="right"><label for="username" class="control-label col-xs-10"><?php echo $lang['Confirm Password'].MANDATORYMARK ?></label></td>
			<td align="left"><div class="col-xs-4"><input title="Enter Confirm Password" class="form-control" type="password"
			name="confirm_password" id="confirm_password" size=25 value=""></div></td>
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"><label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Phone Number'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4"><input title="Enter User Phone Number" class="form-control" type="text"
			name="user_phone" id="user_phone" size=25 value="<?php if($Row[0][0]->user_phone) echo $Row[0][0]->user_phone; else echo $_POST['user_phone'];?>"> </div> </td>
	</tr>
	<tr>
			<td class="fontstyle" align="right"><label for="profile_image" class="control-label col-xs-10"><?php echo $lang['Upload Profile Picture']?></label></td>
			<td align="left"><div class="col-xs-4"><input title="Upload Profile Picture" class="form-control" type="file" name="profile_image" id="profile_image" size=25></div>
			<div class="col-xs-4" style="float:right; border:solid 2px #999;  margin-bottom:3px;">
			<?php if($Row[0][0]->profile_image=='')
			{ 
				$image =  IMAGEURL."profile_picture/no-picture.gif"; 
			} 
			else
				$image = IMAGEURL."profile_picture/".$Row[0][0]->profile_image ; 
			?>
			<img src="<?php echo $image; ?>" title="image"   height="150px" width="150px"/></div>
			<input type="hidden" name="img_name" value="<?php echo $Row[0][0]->profile_image;?> "  />
			</td>
	
	</tr>
    <?php //if($Row[0][0]->role_id =='2') {
	//to fetch all user_id whose info going to edit
	 $r_id = $DBFilter->SelectRecord("users","user_id=".$_GET[id]);
	 //print_r($r_id);
	
		if($_SESSION['usertype'] == 'company_admin' && $r_id->role_id == '3' || $_SESSION['usertype'] == 'supervisor' && $r_id->role_id == '3')
		{
	?>
    <tr> 
		<td align="right" class="fontstyle" width="30%"><label for="username" class="control-label col-xs-10"><?php echo $lang['Assigned To'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4">
       
		<select class="form-control" name="assigned_to">
		<option value="" >Please Select</option>
		<?php 
			for($i=0;$i<count($supervisor[0]);$i++)
			{ ?>
			<option value="<?php echo $supervisor[0][$i]->user_id; ?>" <?php if($Row[0][0]->assigned_to == $supervisor[0][$i]->user_id){?> selected = "selected" <?php }?>><?php echo $supervisor[0][$i]->first_name.' '.$supervisor[0][$i]->last_name; ?></option>
			<?php 
			}
		?>
		</select>
		<?php  } else {?>
			<input type="hidden" class="form-control" name="assigned_to" value="<?php echo $Row[0][0]->assigned_to;?>"><?php } ?>	
		</div></td>
	</tr>
    <?php if($_SESSION['usertype']=='super_admin' || ($Row[0][0]->role_id !=2 && $_SESSION['usertype'] == 'company_admin')) {?>
	<tr> 
		<td align="right" class="fontstyle" width="30%"><label for="active" class="control-label col-xs-10"><?php echo $lang['Active'].MANDATORYMARK?></label></td>
		
		<td align="left"><div class="col-xs-4">
			<input type="radio" name="is_active" id="is_activeY" value="Y" <?php if($Row[0][0]->is_active=='Y') {?> checked <?php } ?> /><?php echo $lang['Active']?>  &nbsp;&nbsp;&nbsp;
			<input type="radio" name="is_active" id="is_activeN" value="N" <?php if($Row[0][0]->is_active=='N') {?> checked <?php } ?>/><?php echo $lang['In-Active']?>
			</div>
		</td>
	</tr><?php } else {?>
    <input type="hidden" name="is_active" value="<?php echo $Row[0][0]->is_active;?>" />
    <?php }?>
	<tr class="alt">
	<td colspan="2">
	<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		
		<button type="submit" class="btn btn-primary" name="update"><?php echo $lang['Update']?></button>
		<!--<button type="submit" class="btn btn-primary" name="clearsearch" onclick="return Clear()"><?php echo $lang['Reset']?></button>-->
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=user');?>'"><?php echo $lang['Back']?></button>
     </div>
		</td>
		</tr>
	</tbody>
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b></div>

	
</form>
</center>