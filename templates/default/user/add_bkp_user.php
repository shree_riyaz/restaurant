<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   User
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/
	$lang = $language->english($lang);
?>
<script>
function Clear()
{	//alert(document.getElementById('role_id')[0].value);
	try{
	document.getElementById('user_phone').value='';
	//alert(document.getElementById('user_phone').value);
	document.getElementById('first_name').value='';
	document.getElementById('last_name').value='';
	document.getElementById('user_email').value='';
	//document.getElementById('confirm_password').value='';
	//document.getElementById('password').value='';
	document.getElementById('role_id').value='';
	//document.getElementById('superviser_row').style.display = 'none';
	if(document.getElementById('assigned_to'))	// to hide Assingned to selectbox on Reset form Added By : Neha Pareek
	{
		document.getElementById('assigned_to').value='';
		document.getElementById('superviser_row').style.display = 'none';
	}
	document.getElementById('profile_image').value='';
	return false;
	}
	catch(e)
	{
		alert("Error: " + e.description);
	}
}
</script>

<br />
<form method="post" name="user_add" id="user_add" enctype="multipart/form-data">
<center>
	<?php

			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?>

 <table width="80%"  border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody>
	<tr valign="middle" align="center">
      <th height="30" class="thColor" colspan="2" style="padding-left: 5px;"><font color="#FFFFFF"><?php echo $lang['Add User']?></font></th>
    </tr>

	<tr>
		<td   colspan="2"  style="font-size:10px; color : red;" align="right"  class="fontstyle">*<?php echo $lang['All fields are mandatory']?></td>
	</tr>
	<?php if ($_SESSION['usertype'] == 'admin') { ?>
		<tr>
		<td align="right" class="fontstyle" width="30%"><label for="username" class="control-label col-xs-10">
		<?php echo $lang['Related To'].MANDATORYMARK?></label></td>
		<td align="left"><div class="col-xs-4">

		<select name="company_id" class="form-control">
		<option value="">Please Select</option>
		<?php

		for($i=0;$i<count($company[0]);$i++)
		{ ?>
		<option value="<?php echo $company[0][$i]->company_id ?>" <?php if($Row->company_id == $company[0][$i]->company_id){?> selected <?php }?>><?php echo $company[0][$i]->company_name ?></option>
		<?php
		}
		?>
</select><?php } else { ?>
		<input class="form-control" type="hidden"
			name="company_id" id="company_id" size=25 value="<?php echo $_SESSION['company_id'];?>">
			<?php } ?>
</div>	</td>
	</tr>
	<tr>
		<td align="right" class="fontstyle" width="30%"><label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['First Name'].MANDATORYMARK?></label></td>
		<td align="left"><div class="col-xs-4"><input title="Enter User First Name" class="form-control" type="text"
			name="first_name" id="first_name" size=25 value="<?php echo $_POST['first_name'];?>"></div>  </td>
	</tr>
	<tr>
		<td align="right" class="fontstyle" width="30%"><label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Last Name'].MANDATORYMARK?></label></td>
		<td align="left"><div class="col-xs-4"><input title="Enter User Last Name" class="form-control" type="text"
			name="last_name" id="last_name" size=25 value="<?php echo $_POST['last_name'];?>"> </div> </td>
	</tr>
	<tr>
			<td class="fontstyle" align="right"><label for="user_email" class="control-label col-xs-10"><?php echo $lang['Email Id'].MANDATORYMARK?></label></td>
			<td align="left"><div class="col-xs-4"><input title="Enter Email Id" class="form-control" type="text"
			name="user_email" id="user_email" size=25 value="<?php echo $_POST['user_email'];?>"></div></td>

	</tr>

   <tr>
		<td align="right" class="fontstyle" width="30%"><label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Phone Number'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
		<input title="Enter User Phone Number" class="form-control" type="text" name="user_phone" id="user_phone" size=25 value="<?php echo $_POST['user_phone'];?>">
		</div>
		<span class="col-xs-10" style="margin-left :5px; font-weight:bold;">(Enter phone no. in (123) 456-7890 or 123-456-7890 or 10 digits format.)</span>
		</td>
	</tr>

	<tr>
		<td align="right" class="fontstyle" width="30%"><label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Role'].MANDATORYMARK ?></label></td>

		<td align="left">
		<div class="col-xs-4">
		<select name="role_id" id="role_id" class="form-control" onchange="this.form.submit()">
		<option value="">Please Select</option>
		<?php
		if($Row[0][0]->user_id != '')//Added By : Neha Pareek. Dated : 02 Dec 2015
		{
			for($i=0;$i<count($roles[0]);$i++)
			{
		?>
			<option value="<?php echo $roles[0][$i]->role_id?>" <?php if($_POST['role_id']== $roles[0][$i]->role_id) {?> selected="selected" <?php } ?>><?php echo $roles[0][$i]->role_name ?></option>
		<?php
			}
		}
		else//Added By : Neha Pareek. Dated : 02 Dec 2015
		{
			for($i=0;$i<count($roles[0]);$i++)
			{
				if($roles[0][$i]->role_id == 4)
				{
		?>
			<option value="<?php echo $roles[0][$i]->role_id?>" <?php if($_POST['role_id']== $roles[0][$i]->role_id) {echo "selected"; } ?>><?php echo $roles[0][$i]->role_name; ?></option>
		<?php
				}
			}
		}
		?>
		</select>
        </div>
        <?php if($Row[0][0]->user_id == '') echo "<span class='col-xs-10' style='margin-left :5px; color : #00688B; font-weight:bold;'>(If no user or no active supervisor present then add supervisor first.)<span>";?>
		</td>
	</tr>
    <?php if($_POST['role_id']=='3') { ?>
    <tr id="superviser_row">
		<td align="right" class="fontstyle" width="30%"><label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Assigned To'].MANDATORYMARK ?></label></td>

		<td align="left"><div class="col-xs-4">
		<select name="assigned_to" id="assigned_to" class="form-control">
		<option value="">Please Select</option>
		<?php
			//echo "<pre>";print_r($supervisor[0]);
		for($i=0;$i<count($supervisor[0]);$i++)
		{
			// if($_POST['assigned_to'] !='')
			// {

		?>
			<option value="<?php echo $supervisor[0][$i]->user_id?>" <?php if($supervisor[0][$i]->user_id == $_POST['assigned_to']){ echo "selected";} ?>><?php echo $supervisor[0][$i]->first_name.' '.$supervisor[0][$i]->last_name; ?></option>
		<?php
			//}
			// else
			// {
		?>

		<?php
			//}
		}
		?>
		</select>
            </div>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td class="fontstyle" align="right"><label for="profile_image" class="control-label col-xs-10"> <?php echo $lang['Upload Profile Picture']?></label></td>
		<td align="left">
		<div>
		<input title="Upload Profile Picture" class="form-control" type="file" name="profile_image" id="profile_image" size=20><span align="right" style='font-weight:bold;' >(upload only : .jpg, .jpeg, .gif and .png image and size should not be more than 2MB.)</span>
		</div></td>
	</tr>
	<tr class="alt">
	<td colspan="2">
	<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		<input type="hidden" name="plan_name" value="<?php echo $planName;?>" />
		<button type="submit" class="btn btn-primary" name="add_user"><?php echo $lang['Add']?></button>
		<button type="reset" class="btn btn-primary" name="clearsearch" onclick="return Clear()"><?php echo $lang['Reset']?></button>
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=user');?>'"><?php echo $lang['Back']?></button>
     </div>
		</td>
	</tr>
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b></div>
</form>
</center>
</body>
<?php
//Added By : Neha Pareek. Dated : 02 Dec 2015
// echo "<pre>";
// print_r($Row);
/*if($Row[0][0]->user_id == '')
{
	echo "<script>alert('You should add atleast a supervisor first.');</script>";
}*/
?>
</html>
