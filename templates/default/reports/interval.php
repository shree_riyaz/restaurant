<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));
//echo "<pre>";
//print_r($Row);
//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="col-sm-9 drop-shadow nopadding">
        <div class="user-heading">
            <span>Interval Wise Report</span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;Interval Based Feedback
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover interval-feedback" id="example">

                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th colspan="8">Time Interval (In Hours)</th>
                                        </tr>
                                        <tr class="text-danger">
                                            <th></th>
                                            <th>12-03</th>
                                            <th>03-06</th>
                                            <th>06-09</th>
                                            <th>09-12</th>
                                            <th>12-15</th>
                                            <th>15-18</th>
                                            <th>18-21</th>
                                            <th>21-24</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        foreach ($Row as $row_list) {
//
                                            ?>


                                            <tr>

                                                <td class="text-center text-success"><span><b><?php echo date('d-m-Y',strtotime($row_list['date'])) ; ?></b></span></td>


                                                <td><span class="text-bold"><b><?php echo  isset($row_list['time_interval']['12-03']) ? $row_list['time_interval']['12-03'] : 0 ; ?></b></span></td>
                                                <td><span class="text-bold"><b><?php echo  isset($row_list['time_interval']['03-06']) ? $row_list['time_interval']['03-06'] : 0 ; ?></b></span></td>
                                                <td><span class="text-bold"><b><?php echo  isset($row_list['time_interval']['06-09']) ? $row_list['time_interval']['06-09'] : 0  ; ?></b></span></td>
                                                <td><span class="text-bold"><b><?php echo  isset($row_list['time_interval']['09-12']) ? $row_list['time_interval']['09-12'] : 0 ; ?></b></span></td>
                                                <td><span class="text-bold"><b><?php echo  isset($row_list['time_interval']['12-15']) ? $row_list['time_interval']['12-15'] : 0 ; ?></b></span></td>
                                                <td><span class="text-bold"><b><?php echo  isset($row_list['time_interval']['15-18']) ? $row_list['time_interval']['15-18'] : 0 ; ?></b></span></td>
                                                <td><span class="text-bold"><b><?php echo  isset($row_list['time_interval']['18-21']) ? $row_list['time_interval']['18-21'] : 0 ; ?></b></span></td>
                                                <td><span class="text-bold"><b><?php echo  isset($row_list['time_interval']['21-23']) ? $row_list['time_interval']['21-23'] : 0 ; ?></b></span></td>

                                            </tr>
                                        <?php } //} ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php for ($i = 1; $i<=$_SESSION['total_page_interval'];$i++){ ?>
            <div class="pagination_weekly_div text-center">

            <nav aria-label="...">
                <ul class="pagination">
                    <?php if($_SESSION['total_page_interval'] > 4 ) { ?>

                    <li class="page-item">
                        <a class="page-link" href="<?php echo ROOTADMINURL.'/index.php?mod=reports&do=interval&page_no=1'; ?>" tabindex="-1">First</a>
                    </li>
                    <?php } ?>

                    <?php
                    for ($i=1;$i<=$_SESSION['total_page_interval'];$i++){ ?>
                        <li class="page-item <?php if($_GET['page_no'] == $i || ($_GET['page_no'] == '' && $i==1 )) echo 'active'; ?> ">
                            <a class="page-link" href="<?php echo ROOTADMINURL.'/index.php?mod=reports&do=interval&page_no='.$i; ?>"><?php echo $i; ?> </a>
                        </li>
                    <?php } ?>
                    <?php if($_SESSION['total_page_interval'] > 4 ) { ?>

                    <li class="page-item">
                        <a class="page-link" href="<?php echo ROOTADMINURL.'/index.php?mod=reports&do=interval&page_no='.$_SESSION['total_page_interval']; ?>" tabindex="-1">Last</a>
                    </li>
                    <?php } ?>

                </ul>
            </nav>
                </div>
            <?php } ?>

        </div>
    </div>
        <input type="hidden" class="company_id_load_more_interval" value="<?php echo $_SESSION['total_page_interval'] ?>">

</section>





