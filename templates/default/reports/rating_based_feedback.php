<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="col-sm-9 drop-shadow nopadding">
        <div class="user-heading">
            <span>Rating Wise Report</span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-star" aria-hidden="true"></i> Breakdown By Feedback Type
                            </div>
                            <?php if (count($get_data_for_pie_feedback_type)){ ?>
                                <div class="panel-body">
                                    <div class="poor-feedback-div">
                                        <!--                        https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/line-basic/-->
                                        <script type="text/javascript">
                                            $(function () {

                                                Highcharts.setOptions({
                                                    colors: ['#CD327D','#99B5FA','#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4','#FFA07A','#52FFF3','#C5D33D','#F30F2F','#F1DD41','#F0974E','#D2A88E','#F7D562','#F88DDF','#B35A8B','#A4743F','#E0E96E','#360C30','#DDFAF8','#FDED04','#F59AAB','#FE4F4F','#FAC58A','#F4D4DA']
                                                });
                                                $('#containers_poor').highcharts({

                                                    chart: {
                                                        plotBackgroundColor: null,
                                                        plotBorderWidth: 0,//null,
                                                        plotShadow: false
                                                    },
                                                    color: {
                                                        radialGradient: { cx: 0.5, cy: 0.5, r: 0.5 },
                                                        stops: [
                                                            [0, '#003399'],
                                                            [1, '#3366AA']
                                                        ]
                                                    },
                                                    title: {
                                                        text: ''
                                                    },
                                                    tooltip: {
                                                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                    },
                                                    plotOptions: {
                                                        pie: {
                                                            allowPointSelect: true,
                                                            cursor: 'pointer',

                                                            dataLabels: {
                                                                enabled: true,
                                                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                                style: {
                                                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                }
                                                            },
                                                            showInLegend: true
                                                        }
                                                    },
                                                    series: [{
                                                        type: 'pie',
                                                        name: 'Feedback',
                                                        size: '90%',
                                                        data: <?php echo json_encode($get_data_for_pie_feedback_type);?>
                                                    }]
                                                });
                                            });

                                        </script>

                                        <div id="containers_poor"></div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="panel-body">
                                    <br>
                                    <div class="text-center">
                                        <span  style="font-size: 15px;">Oops! No data available to show rating based feedback graph chart.</span>
                                    </div>
                                    <br>
                                </div>

                            <?php } ?>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

</section>




<script type="text/javascript">

    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

