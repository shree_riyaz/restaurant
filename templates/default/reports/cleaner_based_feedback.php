<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="col-sm-9 drop-shadow nopadding">
        <div class="user-heading">
            <span>Cleaner Wise Report</span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-tint" aria-hidden="true"></i> Cleaner Based Feedback
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered interval-feedback table-striped table-hover" id="example">
                                        <thead>
                                        <tr>
                                            <th>Cleaner Name</th>
                                            <th>Total Feedback</th>
                                            <th>Excellent</th>
                                            <th>Good</th>
                                            <th>Average</th>
                                            <th>Poor</th>
                                            <th>Very Poor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($Row as $row_list) {
                                            $avg = isset($row_list[0][0]->count) ? $row_list[0][0]->count : 0 ;
                                            $excellent = isset($row_list[0][1]->count) ? $row_list[0][1]->count : 0 ;
                                            $good =    isset($row_list[0][2]->count) ? $row_list[0][2]->count : 0 ;
                                            $poor =    isset($row_list[0][3]->count) ? $row_list[0][3]->count : 0 ;
                                            $very_poor = isset($row_list[0][4]->count) ? $row_list[0][4]->count : 0 ;
                                            $total = $avg+$excellent+$good+$poor+$very_poor;
                                            ?>

                                            <tr>
                                                <td><?php echo isset($row_list['name']) ? $row_list['name'] : 'NA' ?></td>
                                                <td><?php echo $total ?></td>
                                                <td><?php echo $excellent ?></td>
                                                <td><?php echo $good ?></td>
                                                <td><?php echo $avg ?></td>
                                                <td><?php echo $poor ?></td>
                                                <td><?php echo $very_poor ?></td>
                                            </tr>
                                        <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Breakdown By Cleaner Wise
                        </div>
                        <!-- /.panel-heading -->
                        <?php if (count($get_data_for_pie_data_cleaner_pie)){ ?>

                            <div class="panel-body">
                                <div class="">

                                    <script type="text/javascript">

                                        $(function () {

                                            Highcharts.setOptions({
                                                colors: ['#CD327D','#99B5FA','#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4','#FFA07A','#52FFF3','#C5D33D','#F30F2F','#F1DD41','#F0974E','#D2A88E','#F7D562','#F88DDF','#B35A8B','#A4743F','#E0E96E','#360C30','#DDFAF8','#FDED04','#F59AAB','#FE4F4F','#FAC58A','#F4D4DA']
                                            });

                                            $('#cleaner_based_graph_chart').highcharts({

                                                chart: {
                                                    renderTo: 'container',
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: 'Feedback graph on basis of Cleaner staff'
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.y:.f}</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        borderColor: '#000000',
                                                        innerSize: '80%'
                                                    }
                                                },

                                                series: [{
                                                    type: 'pie',
                                                    name: 'Feedback',
                                                    size: '100%',
                                                    innerSize: '60%',
                                                    data: <?php echo json_encode($get_data_for_pie_data_cleaner_pie);?>,
                                                    showInLegend: true,
                                                    dataLabels: {
                                                        enabled: false
                                                    }
                                                }]
                                            });

                                        });

                                    </script>

                                    <div id="cleaner_based_graph_chart"></div>

                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="panel-body">
                                <br>
                                <div class="text-center">
                                    <span  style="font-size: 15px;">Oops! No data available to show cleaner based feedback graph chart.</span>
                                </div>
                                <br>
                            </div>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

