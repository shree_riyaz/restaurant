<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/pie-legend/

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="col-sm-9 drop-shadow nopadding">
        <div class="user-heading">
            <span>Genuine Feedback Report</span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Genuine Based Feedback By Month
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <?php
                                $gen_number = isset($count_data[1]->total_genuine) ? $count_data[1]->total_genuine : 0;
                                $ann_number = isset($count_data[0]->total_anonymous) ? $count_data[0]->total_anonymous : 0;

                                ?>
                                <span style="font-weight: bold; color: darkolivegreen">Count - <?php echo 'Genuine('. $gen_number.') / Anonymous('. $ann_number.')'; ?></span>

                                <br>

                                <!--                                        <span style="font-weight: bold; color: darkolivegreen">Ratio  -  --><?php //echo getRatio($count_data[1]->total_genuine,$count_data[0]->total_anonymous); ?><!--</span>-->
                                <br>
                                <div class="table-responsive ">
                                    <table class="table table-bordered interval-feedback table-striped table-hover" id="example">
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Number of feedback</th>
                                            <th>Excellent</th>
                                            <th>Good</th>
                                            <th>Average</th>
                                            <th>Poor</th>
                                            <th>Very Poor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($Row[0] as $row_list) { ?>

                                            <tr>
                                                <td><?php echo isset($row_list->month_name) ? $row_list->month_name  : 0 ?></td>
                                                <td><?php echo isset($row_list->number_of_feedback) ? $row_list->number_of_feedback  : 0 ?></td>
                                                <td><?php echo isset($row_list->excellent) ? $row_list->excellent  : 0 ?></td>
                                                <td><?php echo isset($row_list->good) ? $row_list->good  : 0 ?></td>
                                                <td><?php echo isset($row_list->avergae) ? $row_list->avergae  : 0 ?></td>
                                                <td><?php echo isset($row_list->poor) ? $row_list->poor  : 0 ?></td>
                                                <td><?php echo isset($row_list->very_poor) ? $row_list->very_poor  : 0 ?></td>

                                            </tr>
                                        <?php } //} ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Genuine vs Anonymous Graph Chart
                        </div>
                        <!-- /.panel-heading -->
                        <?php if ($gen_number > 0 ){ ?>

                            <div class="panel-body">
                                <div class="">
                                    <script type="text/javascript">
                                        $(function () {
                                            Highcharts.setOptions({
                                                colors: ['#CD327D','#99B5FA','#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4','#FFA07A','#52FFF3','#C5D33D','#F30F2F','#F1DD41','#F0974E','#D2A88E','#F7D562','#F88DDF','#B35A8B','#A4743F','#E0E96E','#360C30','#DDFAF8','#FDED04','#F59AAB','#FE4F4F','#FAC58A','#F4D4DA']
                                            });
//                                                Highcharts.chart('container', {
                                            var chart = Highcharts.chart('combined_genuine_vs_anno', {

                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: 'Genuine vs Annonymous comparison chart'
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.y:.f}</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: false
                                                        },
                                                        showInLegend: true
                                                    }
                                                },
                                                series: [{
                                                    name: 'Count',
                                                    colorByPoint: true,
                                                    data: [{
                                                        name: 'Genuine',
                                                        y: <?php echo $count_data[1]->total_genuine ; ?>,

                                                    }, {
                                                        name: 'Annonymous',
                                                        y: <?php echo $count_data[0]->total_anonymous ; ?>,
                                                    }]
                                                }]
                                            });
                                        });
                                    </script>
                                    <div id="combined_genuine_vs_anno"></div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="panel-body">
                                <br>
                                <div class="text-center">
                                    <span  style="font-size: 15px;">Oops! No data available to show genuine based feedback graph chart.</span>
                                </div>
                                <br>
                            </div>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

