<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));
//echo "<pre>";
//print_r($Row);
//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;

?>
<style>text.highcharts-credits {
        display: none;
    }



</style>

<section>
    <div class="col-sm-9 drop-shadow nopadding">
        <div class="user-heading">
            <span>Weekly Wise Report</span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-calendar" aria-hidden="true"></i> Weekly Based Feedback
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive" id="post-data">
                                    <table class="table table-bordered table-striped table-hover interval-feedback" id="example" >

                                        <thead>
                                        <tr>

                                            <th>Week</th>
                                            <th>Total Feedback</th>
                                            <th>Excellent</th>
                                            <th>Good</th>
                                            <th>Average</th>
                                            <th>Poor</th>
                                            <th>Very Poor</th>
                                        </tr>
                                        </thead>
                                        <tbody class="" >

                                        <?php

                                        //echo 444;exit;
                                        foreach ($Row as $row_list_key =>$row_list) {

                                            $get_year_month = explode('-',$row_list_key);
                                            $number_of_week = weeks($get_year_month[1],$get_year_month[0]);


                                            ?>

                                            <tr>
<!--                                                <th colspan="7" class="text-center" style="padding: 10px!important;background: lavender;">--><?php //echo isset($row_list_key) ? $row_list_key : 'NA'; ?><!--</th>-->
                                                <th colspan="7" class="text-center" style="padding: 10px!important;background: lavender;"><?php echo isset($row_list_key) ? strtoupper(date('Y - F',strtotime($row_list_key.'-01'))): 'NA'; ?></th>
                                            </tr>

                                            <?php

                                            $j = $number_of_week+1;
//                                                for($i=0; $i< count($row_list); $i++) {
                                            for($i=0; $i< $number_of_week; $i++) {
                                                $week_list = get_list_of_all_week(date('Y-m-d',strtotime($row_list_key.'-01')));
//                                                foreach($week_list as $week_list_key => $week_list_val){
                                                $explode_date = explode('-',date('Y-m-W',strtotime($row_list_key.'-01')));
//                                                if($row_list[$i]->number_of_feedback > 0){
//                                                if(in_array($explode_date[2],$week_list) && $row_list[$i]->number_of_feedback > 0){
//                                                    echo $explode_date[2].'-';
//                                                    echo $key = array_search($explode_date[2], $week_list);


//                                                if($row_list[$i]->number_of_feedback > 0){
                                                $j--;
//                                               echo '<pre>'; print_r($row_list[$i]->number_of_feedback.'--'. $j.'--'.$row_list_key);
//                                                echo $row_list[$i]->number_of_feedback.'--'. $j.'--'.$row_list[$i]->years;
//                                               echo '<pre>'; print_r($week_list);
//                                                echo $j;
                                                ?>


                                                <tr>

                                                    <td class="text-center" ><?php echo 'WEEK- '.$j ; ?></td>

                                                    <!--            <td class="text-center">--><?php //echo 'Week'.$i ; ?><!--</td>-->
                                                    <td class="text-center"><?php echo isset($row_list[$i]->number_of_feedback) ? $row_list[$i]->number_of_feedback : 0; ?></td>
                                                    <td class="text-center"><?php echo isset($row_list[$i]->excellent) ? $row_list[$i]->excellent : 0; ?></td>
                                                    <td class="text-center"><?php echo isset($row_list[$i]->good) ? $row_list[$i]->good : 0; ?></td>
                                                    <td class="text-center"><?php echo isset($row_list[$i]->avergae) ? $row_list[$i]->avergae : 0; ?></td>
                                                    <td class="text-center"><?php echo isset($row_list[$i]->poor) ? $row_list[$i]->poor : 0; ?></td>
                                                    <td class="text-center"><?php echo isset($row_list[$i]->very_poor) ? $row_list[$i]->very_poor : 0; ?></td>

                                                </tr>

                                            <?php  } } //} //} ?>

                                        </tbody>
                                    </table>
                                    <?php if($_SESSION['total_page_weekly'] > 1 ) { ?>
                                    <div class="pagination_weekly_div text-center">
                                        <nav aria-label="...">
                                        <ul class="pagination">

                                            <?php if($_SESSION['total_page_weekly'] > 4 ) { ?>

                                            <li class="page-item">
                                                <a class="page-link" href="<?php echo ROOTADMINURL.'/index.php?mod=reports&do=weekly_interval&page_no=1'; ?>" tabindex="-1">First</a>
                                            </li>
                                            <?php } ?>
                                            <?php
                                            for ($i=1;$i<=$_SESSION['total_page_weekly'];$i++){ ?>
                                                <li class="page-item <?php if($_GET['page_no'] == $i || ($_GET['page_no'] == '' && $i==1 ) ) echo 'active'; ?> ">
                                                    <a class="page-link" href="<?php echo ROOTADMINURL.'/index.php?mod=reports&do=weekly_interval&page_no='.$i; ?>"><?php echo $i; ?> </a>
                                                </li>
                                            <?php } ?>
                                            <?php if($_SESSION['total_page_weekly'] > 4 ) { ?>

                                            <li class="page-item">
                                                <a class="page-link" href="<?php echo ROOTADMINURL.'/index.php?mod=reports&do=weekly_interval&page_no='.$_SESSION['total_page_weekly']; ?>" tabindex="-1">Last</a>
                                            </li>
                                            <?php } ?>

                                        </ul>
                                        </nav>
                                    </div>
                                    <?php } ?>

                                </div>

                            </div>

                        </div>

                       <!-- <div class="ajax-load text-center" style="display:none">

                            <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>

                        </div>-->
                    </div>

                </div>
            <?php } ?>
        </div>

    </div>

    <input type="hidden" class="company_id_load_more" value="<?php echo $_SESSION['total_page_weekly'] ?>">


</section>


<?php

// include_once(JS . "/app.js");

?>


