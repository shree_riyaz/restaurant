<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Admin Panel</title>

<?php echo "<link rel='stylesheet' type='text/css' href='".STYLE."/exam.css' />"; ?>
</head>
<body onload="document.loginform.loginid.focus();">

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<? include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <!--<tr>
		<td>
			<div id="searchbar">
				<form action="" method="post" name="frmsearch" id="frmsearch">
					<input name="search" type="text" class="rounded" id="search"
						onfocus="if (this.value == 'Site Search'){this.value = '';}"
						onblur="if (this.value == ''){this.value = 'Site Search';}" size="30"
					 />
					<input name="searchbutton" type="button" value="Go" id="gobutton"/>
				</form>
			</div>
		</td>
	  </tr>-->
	  <tr>
		<td>
			<div id="content" style="height:380px;">
			<div class="border_blue rounded" style="height:368px;">
			<h2>Online Examination Administration Login</h2>
			<div id="msg">&nbsp;<?php ShowMessage();?></div>
				<form id="loginform" name="loginform" method="post" action="">
				  <table width="343" border="0" cellpadding="2" cellspacing="2" align="center" id="logintable" style="border:1px solid #ccc;">
					<tr>
					  <td height="30" colspan="4" bgcolor="#6ba6d0"><div align="center" style="color:#FFFFFF; font-weight:bold; font-size:14px;">Login</div></td>
					</tr>
					<tr>
					  <td colspan="4">&nbsp;</td>
					</tr>

					<tr>
					  <td width="106"><div align="right"><span class="style3">Login Id </span>: </div></td>
					  <td width="226" colspan="3">
						<input name="loginid" id="loginid" type="text" size="25" class="rounded textfield" />         </td>
				    </tr>
					<tr>
					  <td><div align="right"><span class="style3">Password </span>:</div></td>
					  <td colspan="3"><input name="password" id="password" type="password" size="25" class="rounded textfield" /></td>
				    </tr>
					<tr>
					  <td colspan="4" align="center">
					  	<input type="submit" name="login" value="Login"  class="buttons rounded"  />&nbsp;
					  	<input type="reset" name="reset" value="Reset" class="buttons rounded" onclick="document.loginform.loginid.focus();document.getElementById('msg').innerHTML='&nbsp;'"/></td>
				    </tr>
					<tr>
					  <td colspan="4" align="center">&nbsp;</td>
				    </tr>
				  </table>
			  </form>

				</div>
			</div><!--Content div closed-->
		</td>
	  </tr>

	</table>

</div><!--Outer wrapper closed-->
<?php include_once(CURRENTTEMP."/"."footer.php"); ?>

</body>
</html>