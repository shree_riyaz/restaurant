<?php
	/*======================================
	Developer	-	JAishree Sahal
	Module      -   Company
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
	$lang = $language->english($lang);
	//echo "<pre>";
	//print_r($plan[0]);
	//exit;
	//echo "<pre>";print_r($frmdata);
			/*print_r($_SESSION);
			print_r($_POST);
			*/
?>
<script>
 $(function() {
	var dateToday = new Date();
	var yrRange = dateToday.getFullYear() + ":" + (dateToday.getFullYear() + 15); // year drop down with current year to next 15 year Added By : Neha Pareek Dated : 10-11-2015
		$("#creation_date").datepicker({
		dateFormat: 'yy-mm-dd',
			minDate: 0,
			changeMonth: true,changeYear: true, yearRange: yrRange//added by : Neha Pareek. Dated : 10-11-2015	
		});
		$("#expiry_date").datepicker({
			dateFormat: 'yy-mm-dd' ,minDate: 0,
			changeMonth: true, changeYear: true, yearRange: yrRange //added by : Neha Pareek. Dated : 10-11-2015
		});
	});
function Clear()
{
	document.getElementById('user_name').value='';
	document.getElementById('company_name').value='';
	document.getElementById('creation_date').value='';
	document.getElementById('expiry_date').value='';
	document.getElementById('subscription_plan').value='';
	document.getElementById('company_email').value='';
	location.href="index.php?mod=company&do=add";
	return false;
}
</script>
<br />
<br />	
<form method="post" name="company_add" id="company_add" enctype="multipart/form-data">

	<?php 
				
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
	?>

 <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody><tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2" style="padding-left:5px;"><font color="#FFFFFF"><?php echo $lang['Add New Company']?></font></th>
    </tr>
	<tr>
		<td   colspan="2"  style="font-size:10px; color : Red;" align="right"  class="fontstyle">*<?php echo $lang['All fields are mandatory']?></td>
	</tr>
	
	<tr> 
		<td class="fontstyle" align="right">
		 <div class="form-group">
            <label for="username" class="control-label col-xs-10"><?php echo $lang['Company Name'].MANDATORYMARK?></label>
        </td>
		<td align="left">
            <div class="col-xs-4">
		   <input type="text" title="Enter Company Name" class="form-control" id="company_name" name="company_name" value="<?php echo $_POST['company_name'];?>" onbuler="return  KeywordSearch()" >
            </div>
        </td>
     </div>
		
		
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="username" class="control-label col-xs-10">
		<?php echo $lang['Contact Person'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
		   <input type="text" title="Enter User Name" class="form-control" id="user_name" name="user_name" value="<?php echo $_POST['user_name'];?>"> </div> </td>
	</tr>
	<tr> 
		<td align="right" class="fontstyle"> <label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Subscription Plan'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
           <select class="form-control" name="plan_id" id="subscription_plan" style="width:170px;">		
			<option value="">Please Select</option>
			<?php //echo"<pre>"; print_r($plan[0]);
			for($i=0;$i<count($plan[0]);$i++) 
			{ //echo"<pre>"; echo $plan[0][$i];
			?>
			<option value="<?php echo $plan[0][$i]->plan_id?>" <?php if($plan[0][$i]->plan_id == $_POST['plan_id']) { echo "selected";} ?>><?php echo $plan[0][$i]->plan_name; ?></option>
			<?php } ?>
		</select>
		</td>
	</tr>
	
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="creation_date" class="control-label col-xs-10"><?php echo $lang['Creation Date'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
		   <input type="text" title="Enter Creation Date" class="form-control" id="creation_date"  name="creation_date" value="<?php echo $_POST['creation_date'];?>">
            </div> </td>
		
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="expiry_date" class="control-label col-xs-10"><?php echo $lang['Expiry Date'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
		   <input type="text" title="Enter Expiry Date" class="form-control" id="expiry_date" name="expiary_date" value="<?php echo $_POST['expiary_date'];?>"> </div>
		</td>
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="company_email" class="control-label col-xs-10">
		<?php echo $lang['Email Id'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
		   <input type="text" title="Enter Email Id" class="form-control" id="company_email" name="company_email" value="<?php echo $_POST['company_email'];?>"> </div>
		</td>
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="company_logo" class="control-label col-xs-10">
		<?php echo $lang['Company Logo'].MANDATORYMARK?></label></td>
		<td align="left">
		<div>
		   <input type="file" title="Enter Company Logo" class="form-control" id="company_logo" name="company_logo"><span align="right" >(upload only : .jpg, .jpeg, .gif and .png image and size should not be more than 2MB.)</span>
		</div>
		</td>
	</tr>
	<tr class="alt">
		<td colspan=2 style="text-align: center;" align="left">
		<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		
		<button type="submit" class="btn btn-primary" name="add_company"><?php echo $lang['Add']?></button>
		<button type="submit" class="btn btn-primary" name="clearsearch" onclick="return Clear()"><?php echo $lang['Reset']?></button>
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=company');?>'"><?php echo $lang['Back']?></button>
     </div>
		</td>
	</tr>
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b></div>

	
</form>

</center>
</body>

</html>
