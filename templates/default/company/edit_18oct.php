
<script type="text/javascript" language="javascript">
/*$(function() {
		$("#creation_date").datepicker({dateFormat: 'yy-mm-dd',
		minDate: 0
		});
		$("#expiry_date").datepicker({dateFormat: 'yy-mm-dd' ,minDate: 0});
});*/
//added By : Neha Pareek. Dated : 14-12-2015
 $(function() {
	var dateToday = new Date();
	var yrRange = dateToday.getFullYear() + ":" + (dateToday.getFullYear() + 15); // year drop down with current year to next 15 year Added By : Neha Pareek Dated : 10-11-2015
		$("#creation_date").datepicker({
		dateFormat: 'yy-mm-dd',
			minDate: 0,
			changeMonth: true,changeYear: true, yearRange: yrRange//added by : Neha Pareek. Dated : 10-11-2015	
		});
		$("#expiry_date").datepicker({
			dateFormat: 'yy-mm-dd' ,minDate: 0,
			changeMonth: true, changeYear: true, yearRange: yrRange //added by : Neha Pareek. Dated : 10-11-2015
		});
	});
function Reset() {
    document.getElementById("company_edit").reset();
}
function Clear()
{
	var id = <?php echo $_GET['id'];?>;
	document.getElementById('user_name').value='';
	document.getElementById('company_name').value='';
	document.getElementById('creation_date').value='';
	document.getElementById('expiry_date').value='';
	document.getElementById('subscription_plan').value='';
	document.getElementById('company_email').value='';
	location.href="index.php?mod=company&do=edit&id=".id;
	return false;
}
</script>
<center>
<br />
		

<br />	
<form method="post" name="company_edit" id="company_edit" enctype="multipart/form-data">
<center>
	<?php 
		
			//print_r($Row);
			$lang = $language->english('eng');
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?>
<?php
// print_r($Row);exit;
?>
 <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody><tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2" style="padding-left:5px;"><font color="#FFFFFF"><?php echo $lang['Edit Company']?></font></th>
    </tr>
	<tr>
		<td   colspan="2"  style="font-size:10px; color : Red;" align="right"  class="fontstyle">*<?php echo $lang['All fields are mandatory']?></td>
	</tr>
	
	<tr> 
		<td class="fontstyle" align="right">
		 <div class="form-group">
            <label for="username" class="control-label col-xs-10"><?php echo $lang['Company Name'].MANDATORYMARK?></label></td>
		<td align="left"><div class="col-xs-4">
		   <input type="text" title="Enter Company Name" class="form-control" id="company_name" name="company_name" 
           value="<?php if($_POST['company_name']) echo $_POST['company_name']; else echo $Row->company_name; ?>"> </div>
     </div>
		
		
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="username" class="control-label col-xs-10"><?php echo $lang['Contact Person'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
		   <input type="text" title="Enter User Name" class="form-control" id="user_name" name="user_name" value ="<?php if($_POST['user_name']) echo $_POST['user_name']; else echo $Row->user_name;?>"> </div> </td>
	</tr>
	<tr> 
		<td align="right" class="fontstyle"> <label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Subscription Plan'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
           <select class="form-control" name="plan_id" id="subscription_plan" style="width:170px;">		
			<option value="" >Please Select</option>
            <?php for($i=0;$i<count($plan[0]);$i++) 
			{ 
			?>
			<option value="<?php echo $plan[0][$i]->plan_id?>" <?php if($Row->plan_id == $plan[0][$i]->plan_id) { echo "selected"; } elseif($Row->plan_id == $_POST['plan_id']) echo "selected"; ?>><?php echo $plan[0][$i]->plan_name; ?></option>
			<?php } ?>
		</select>
		</td>
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="creation_date" class="control-label col-xs-10"><?php echo $lang['Creation Date'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
		   <input type="text" title="Enter Creation Date" class="form-control"  id="creation_date" name="creation_date" value="<?php echo $Row->creation_date; ?>"> </div> </td>
		
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="expiry_date" class="control-label col-xs-10"><?php echo $lang['Expiry Date'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
		   <input type="text" title="Enter Expiary Date" class="form-control"   id="expiry_date" name="expiary_date" value="<?php echo $Row->expiary_date; ?>"> </div>
		</td>
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="emailId" class="control-label col-xs-10"><?php echo $lang['Email Id'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
		   <input type="text" title="Enter Email Id" class="form-control" id="company_email" name="company_email" value="<?php if($_POST['company_email']) echo $_POST['company_email']; else echo $Row->company_email;?>"> </div>
		</td>
	</tr>
    <tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="company_logo" class="control-label col-xs-10">
		<?php echo $lang['Company Logo'].MANDATORYMARK?></label></td>
		<td align="left">
		<div style="float :left;">
		<input title="Upload Company Logo" class="form-control" type="file" name="company_logo" id="company_logo" size=20>
		</div>
		<div style="float : right; width : 50%;">
		(upload only : .jpg, .jpeg, .gif and .png image and size should not be more than 2MB.)
		</div>
		<div class="col-xs-4" style=" border:solid 2px #999;  margin-bottom:3px; margin-top : 5px; clear : both;">
		<?php
			if($Row->company_logo=='')
			{ 
				$image =  IMAGEURL."company_logo/no-picture.gif"; 
			} 
			else
				 $image = IMAGEURL."company_logo/".$Row->company_logo ; 
		?>
			<img src="<?php echo $image; ?>" title="image"   height="150px" width="150px">
		</div>
			
			<input type="hidden" name="logo" value="<?php echo $Row->company_logo;?> "  />
		</td>
	</tr>
  
    <tr> 
		<td align="right" class="fontstyle" width="30%"><label for="active" class="control-label col-xs-10"><?php echo $lang['Active'].MANDATORYMARK?></label></td>
		
		<td align="left"><div class="col-xs-6">
			<input type="radio" name="is_active" value="Y" <?php if($Row->is_active=='Y') {?> checked <?php } ?> /><?php echo $lang['Active']?>  &nbsp;&nbsp;&nbsp;
			<input type="radio" name="is_active" value="N" <?php if($Row->is_active=='N') {?> checked <?php } ?>/><?php echo $lang['In-Active']?>
			</div>
		</td>
	</tr>
	<tr class="alt">
		<td colspan=2 style="text-align: center;" align="left">
		<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		
		<button type="submit" class="btn btn-primary" name="update"><?php echo $lang['Update']?></button>
		<!--<button type="submit" class="btn btn-primary" name="clearsearch" onclick="return Clear()"><?php echo $lang['Reset']?></button>	-->
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=company');?>'"><?php echo $lang['Back']?></button>
     </div>
		</td>
	</tr>
	
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b></div>

</form>
</center>