

    <section>

        <div class="col-sm-9 drop-shadow nopadding">

            <a href="#" data-toggle="modal" data-target="#myModal" class="delete-modal" ><i class="fa fa-times" aria-hidden="true"></i></a>

            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center">Are you sure?</h4>
                        </div>
                        <div class="modal-body text-center">
                            <p class="modal-message-p">Do you really want to delete these records?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <!--                <button type="button"  class="btn btn-danger" data-dismiss="modal">Delete</button>-->
                            <a href="#" class="btn btn-danger delete-href">Delete</a>
                        </div>
                    </div>

                </div>
        </div>
        </div>



    </section>