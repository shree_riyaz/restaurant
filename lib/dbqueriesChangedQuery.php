<?php
/*=======================================================================
	@Auther 	Niteen Achray
	@Date   	18/03/2007
	@Company	Sunarc Technologies
//======================================================================= */
defined("ACCESS") or die("Access Restricted");

/*###########################################################################################
@para	: members record object
@return : session object
@Des 	: to set session for a member

//###########################################################################################*/

function CreateSession($session)
{
	global $frmdata; $_SESSION
	
	//if(isset($_SESSION['auth']))
	$data['sessiondata']=serialize($session);
	$data['sessionid']=session_id();
	$data['sessiondate']=date('Y-m-d');

	InsertRecord('sessiondetails',$data);
	{
	$_SESSION['auth']=serialize($session);
	}
	return SetSession();
}


/*###########################################################################################
@para	: --
@return : sesson object
@Des 	: to set session data

//###########################################################################################*/

function SetSession()
{
	
	global $_SESSION;
	
	$sessionRecord = SelectRecord('sessiondetails',"sessionid='".session_id()."'");
	
	if($sessionRecord && isset($_SESSION['auth']))
	{
	
		return (object) @unserialize($_SESSION['auth']);
	}
	else
	{
		return false;
	}	
}


/*###########################################################################################
@para	: --
@return : ---
@Des 	: to update session after changeing data

//###########################################################################################*/

function UpdateSession()
{
	global $_SESSION;
	global $session;
	//print_r($_SESSION);
	$session=SelectRecord('members',"memid='".$session->
memid."'");
	
	UpdateRecord('sessiondetails',array("sessiondata='".serialize($session)."'"),"sessionid='".session_id()."'");
	$_SESSION['auth']=serialize($session);
		
}

function getLoginCredentials()
{
	global $frmdata;
	$result=SelectRecord('user',"user_name='".$frmdata['username']."' ");

	return $result;
}


function getDevices(&$totalCount,$orderby='')
{

global $frmdata;
$flag=0;
/*$query="select d.id id ,
       d.device_id, 
       d.make,
       d.model,
       m.id m_id,
       m.company_name,
       mdl.id mdl_id,
       mdl.model_name,
       count(dc.component_id) as totalComponent
from device d
     left join manufacturers m on m.id = d.make and (m.manufacturer_for='device' or m.manufacturer_for='all')
     left join models mdl on mdl.id = d.model  
     left join device_components dc on dc.device_id = d.id";*/

//Query changed by Bajrang.
$query="select d.id id,
       d.device_id as device_name,
       d.device_id,
       d.make,
       d.model,
       m.id m_id,
       m.company_name,
       mdl.id mdl_id,
       mdl.model_name,
       count(distinct dc.component_id) as totalComponent,
       
       /*sum(IF(ckf.key_factor_field_type = 'drop down',kv.kg_co2*dckf.quantity,
       dckf.key_factor_value
       )) as totalCo2 */
       
      /* 
       sum(IF (ckf.key_factor_field_type = 'drop down', kv.kg_co2 *
       dckf.quantity * if(ckf.kg_co2>0,ckf.kg_co2,1),
       dckf.key_factor_value  * if(ckf.kg_co2>0,ckf.kg_co2,1) )) as totalCo2  
       */
                

 sum( 
        if(
          IF (ckf.key_factor_field_type = 'drop down', kv.kg_co2 *
         dckf.quantity * if (ckf.kg_co2 > 0, ckf.kg_co2, 1),
          dckf.key_factor_value * if (ckf.kg_co2 > 0, ckf.kg_co2, 1)) > 0,
          
          IF (ckf.key_factor_field_type = 'drop down', kv.kg_co2 *
         dckf.quantity * if (ckf.kg_co2 > 0, ckf.kg_co2, 1),
          dckf.key_factor_value * if (ckf.kg_co2 > 0, ckf.kg_co2, 1))          
          ,0)
          
          +
          
		if(kv_additional.id>0, CONVERT(  SUBSTRING_INDEX(SUBSTRING_INDEX(dc.additional_quantity, ',', (FIND_IN_SET(kv_additional.id, dc.additionalicchip_ids))), ',', - 1) 
 				,UNSIGNED INTEGER ) * (kv_additional.kg_co2),0)          
 )    as totalCo2     
       
from device d
     left join manufacturers m on m.id = d.make and (m.manufacturer_for =
      'device' or m.manufacturer_for = 'all')
     left join models mdl on mdl.id = d.model
     left join device_components dc on dc.device_id = d.id
     
     left join component_key_factor ckf on ckf.component_id = dc.component_id and ckf.status='A'
     left join device_component_key_factors dckf on dckf.component_id = dc.component_id 
 		    and dckf.key_factor_id = ckf.id and dckf.device_id = d.id
     
     left join kf_values kv on kv.id = dckf.key_factor_value 
	     	and ckf.key_factor_field_type = 'drop down' and kv.status='A'
     
      left join kf_values kv_additional on  FIND_IN_SET(kv_additional.id,dc.additionalicchip_ids)  and
       ckf.key_factor_field_type = 'drop down' and kv_additional.status = 'A'
       
/* group by d.id , d.id,m.id,mdl.id,dc.id,  ckf.id,dckf.id, kv.id */";

if(isset($frmdata['make']) && $frmdata['make']!='ps' && $frmdata['make'] !='' || isset($frmdata['model']) && $frmdata['model']!='ps' && $frmdata['model'] != '')
{
	$query.=" where 1 ";
	if(isset($frmdata['make']) && $frmdata['make']!='ps' and trim($frmdata['make'])!='')
	{	
		 $query.=" and d.make = '".$frmdata['make']."' ";
		$flag=1;
	
	}
	if(isset($frmdata['model']) && $frmdata['model']!='ps' and trim($frmdata['model']) != '')
	{	
		$query.=" and d.model = '".$frmdata['model']."' ";
		$flag=1;
	
	}
}

$query.=" group by d.id ";

	if($orderby!='')
		$query.=" order by ".$orderby;
	else 
	    $query.=" order by id desc ";
	    
  //echo $query;
	$result=RunSelectQueryWithPagination($query,$totalCount);
	return $result;
}

?>
